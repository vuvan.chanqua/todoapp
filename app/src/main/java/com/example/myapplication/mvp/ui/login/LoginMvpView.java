package com.example.myapplication.mvp.ui.login;


import com.example.myapplication.mvp.ui.base.MvpView;


public interface LoginMvpView extends MvpView {

    void loginSuccess();

    void loginFailed();
}
