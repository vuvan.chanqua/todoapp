

package com.example.myapplication.mvp.di.component;

import android.app.Application;
import android.content.Context;

import com.example.myapplication.mvp.MvpApp;
import com.example.myapplication.mvp.data.DataManager;
import com.example.myapplication.mvp.di.ApplicationContext;
import com.example.myapplication.mvp.di.module.ApplicationModule;
import com.example.myapplication.mvp.service.SyncService;

import javax.inject.Singleton;

import dagger.Component;



@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(MvpApp app);

    void inject(SyncService service);

    @ApplicationContext
    Context context();

    Application application();

    DataManager getDataManager();
}