package com.example.myapplication.mvp.ui.login;


import com.example.myapplication.mvp.di.PerActivity;
import com.example.myapplication.mvp.ui.base.MvpPresenter;


@PerActivity
public interface LoginMvpPresenter<V extends LoginMvpView> extends MvpPresenter<V> {

    void login(String email, String password);
}
