

package com.example.myapplication.mvp;

import android.app.Application;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.interceptors.HttpLoggingInterceptor.Level;
import com.example.myapplication.mvp.data.DataManager;
import com.example.myapplication.mvp.di.component.ApplicationComponent;
import com.example.myapplication.mvp.di.component.DaggerApplicationComponent;
import com.example.myapplication.mvp.di.module.ApplicationModule;
import com.example.myapplication.mvp.utils.AppLogger;

import com.google.gson.Gson;
import javax.inject.Inject;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;




public class MvpApp extends Application {

    @Inject
    DataManager mDataManager;

    @Inject
    CalligraphyConfig mCalligraphyConfig;

    private ApplicationComponent mApplicationComponent;
    private static MvpApp mSelf;
    private Gson mGSon;

    public static MvpApp self() {
        return mSelf;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this)).build();

        mApplicationComponent.inject(this);

        AppLogger.init();

        AndroidNetworking.initialize(getApplicationContext());
        if (BuildConfig.DEBUG) {
            AndroidNetworking.enableLogging(Level.BODY);
        }

        CalligraphyConfig.initDefault(mCalligraphyConfig);
        mSelf = this;
        mGSon = new Gson();
    }

    public ApplicationComponent getComponent() {
        return mApplicationComponent;
    }


    // Needed to replace the component with a test specific one
    public void setComponent(ApplicationComponent applicationComponent) {
        mApplicationComponent = applicationComponent;
    }
    public Gson getGSon() {
        return mGSon;
    }
}
