package com.example.myapplication.mvp.data.firebase;

import java.io.Serializable;

public class Workspace implements Serializable {
    private String id;
    private String name;
    private String img_url;
    private String user_id;

    public Workspace() {
    }

    public Workspace(String id, String name, String img_url, String user_id) {
        this.id = id;
        this.name = name;
        this.img_url = img_url;
        this.user_id = user_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    @Override
    public String toString() {
        return name;
    }
}
