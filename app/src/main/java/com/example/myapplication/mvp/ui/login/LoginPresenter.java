package com.example.myapplication.mvp.ui.login;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.myapplication.mvp.data.DataManager;
import com.example.myapplication.mvp.ui.base.BasePresenter;
import com.example.myapplication.mvp.utils.rx.SchedulerProvider;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;



public class LoginPresenter<V extends LoginMvpView> extends BasePresenter<V>
        implements LoginMvpPresenter<V> {

    private static final String TAG = "LoginPresenter";
    private FirebaseAuth mAuth;

    @Inject
    public LoginPresenter(DataManager dataManager,
                          SchedulerProvider schedulerProvider,
                          CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
        this.mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void login(String email, String password) {
        getMvpView().showLoading();
        Log.d("LOGIN_TAG", email + " : " + password);
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            LoginPresenter.this.getMvpView().hideLoading();
                            LoginPresenter.this.getMvpView().loginSuccess();
                        } else {
                            LoginPresenter.this.getMvpView().hideLoading();
                            LoginPresenter.this.getMvpView().loginFailed();
                        }
                    }
                });
    }

}
