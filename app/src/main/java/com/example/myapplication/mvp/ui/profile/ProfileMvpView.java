package com.example.myapplication.mvp.ui.profile;

import com.example.myapplication.mvp.data.firebase.User;
import com.example.myapplication.mvp.ui.base.MvpView;

public interface ProfileMvpView extends MvpView {
    void loadUserInformation(User userInfo);

    void checkPasswordSuccess();


    void saveInformationSuccess();

    void saveInformationFailed();
}