package com.example.myapplication.mvp.ui.register;

import com.example.myapplication.mvp.ui.base.MvpView;

public interface RegisterMvpView extends MvpView {
    void registerSuccess();

    void registerFailed();
}