package com.example.myapplication.mvp.ui.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myapplication.mvp.R;
import com.example.myapplication.mvp.ui.base.BaseActivity;
import com.example.myapplication.mvp.ui.main.MainActivity;
import com.example.myapplication.mvp.ui.register.RegisterActivity;
import com.google.firebase.auth.FirebaseAuth;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.myapplication.mvp.utils.AppConstants.EMAIL_PATTERN;
import static com.example.myapplication.mvp.utils.AppConstants.REQUEST_CODE_EXAMPLE;


public class LoginActivity extends BaseActivity implements LoginMvpView {

    @Inject
    LoginMvpPresenter<LoginMvpView> mPresenter;

    @BindView(R.id.usnText)
    EditText usnText;

    @BindView(R.id.pwdText)
    EditText pwdText;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, LoginActivity.class);
    }

    @OnClick(R.id.loginBtn)
    public void onLogin(View view) {
        if (TextUtils.isEmpty(usnText.getText().toString())) {
            usnText.setError("Cannot empty");
        }

        if (TextUtils.isEmpty(pwdText.getText().toString())) {
            pwdText.setError("Cannot empty");
        }
        if (!TextUtils.isEmpty(usnText.getText().toString()) && !TextUtils.isEmpty(pwdText.getText().toString())) {
            if (usnText.getText().toString().matches(EMAIL_PATTERN)) {
                mPresenter.login(usnText.getText().toString(), pwdText.getText().toString());
            } else {
                usnText.setError("Wrong format");
            }

        }
    }

    @OnClick(R.id.signupBtn)
    public void onRegister(View view) {
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivityForResult(intent, REQUEST_CODE_EXAMPLE);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));

        mPresenter.onAttach(LoginActivity.this);

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null) {
            loginSuccess();
        }
        setUp();
    }


    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_EXAMPLE) {
            if (resultCode == Activity.RESULT_OK) {
                Intent i = getIntent();
                final String email = data.getStringExtra(RegisterActivity.EXTRA_EMAIL);
                final String password = data.getStringExtra(RegisterActivity.EXTRA_PASSWORD);
                usnText.setText(email);
                pwdText.setText(password);
            }
        }
    }

    @Override
    public void loginSuccess() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    @Override
    public void loginFailed() {
        Toast.makeText(this, "Login failed", Toast.LENGTH_SHORT).show();
    }
}
