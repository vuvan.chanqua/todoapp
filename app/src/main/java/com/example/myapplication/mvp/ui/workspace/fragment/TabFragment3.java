package com.example.myapplication.mvp.ui.workspace.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.myapplication.mvp.R;
import com.example.myapplication.mvp.data.firebase.Task;
import com.example.myapplication.mvp.data.firebase.User;
import com.example.myapplication.mvp.data.firebase.Workspace;
import com.example.myapplication.mvp.ui.add_note.AddNoteActivity;
import com.example.myapplication.mvp.ui.base.BaseFragment;
import com.example.myapplication.mvp.ui.main.MainMvpPresenter;
import com.example.myapplication.mvp.ui.main.MainMvpView;
import com.example.myapplication.mvp.ui.main.adapter.CustomAdapter;
import com.example.myapplication.mvp.ui.main.adapter.CustomButtonListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;

import static com.example.myapplication.mvp.utils.AppConstants.REQUEST_CODE_EXAMPLE1;

public class TabFragment3 extends BaseFragment implements MainMvpView, CustomButtonListener {

    private static final String WORKSPACE_OBJECT = "WORKSPACE_OBJECT";

    @Inject
    MainMvpPresenter<MainMvpView> mPresenter;

    Button button;
    ImageView deleteBtn, editBtn;
    ListView listView;
    CustomAdapter customAdaper;
    ArrayList<Task> arrTasks = new ArrayList<>();
    FirebaseAuth mAuth;
    FirebaseUser currentUser;
    Spinner spnWp;
    ProgressDialog progressDialog;
    Workspace mWorkspace;

    private static final int PICK_IMAGE = 1;
    private String imgUri = "";
    private String id = "-1";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        setUp(view);
    }

    @Override
    protected void setUp(View view) {
        button = view.findViewById(R.id.button);
        listView = (ListView) view.findViewById(R.id.listItem);
        deleteBtn = (ImageView) view.findViewById(R.id.delete_btn);
        editBtn = (ImageView) view.findViewById(R.id.edit_btn);
        spnWp = (Spinner) view.findViewById(R.id.spnWp);

        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this, view));
        mPresenter.onAttach(TabFragment3.this);

        final Intent data = getActivity().getIntent();
        final Workspace workspace = (Workspace) data.getSerializableExtra(WORKSPACE_OBJECT);
        if (workspace != null) {
            mWorkspace = workspace;
        }
        mWorkspace = workspace;
        mPresenter.getAllWorkspaceTask(workspace);

        customAdaper = new CustomAdapter(getActivity(), R.layout.item_note, arrTasks);
        customAdaper.setCustomButtonListner(TabFragment3.this);
        listView.setAdapter(customAdaper);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AddNoteActivity.class);
                intent.putExtra("activity", "workspace");
                intent.putExtra(WORKSPACE_OBJECT, mWorkspace);
                startActivityForResult(intent, REQUEST_CODE_EXAMPLE1);
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tab_fragment_3, container, false);
    }

    @Override
    public void openLoginActivity() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_EXAMPLE1) {
            if (resultCode == Activity.RESULT_OK) {
                Intent i = getActivity().getIntent();
                mPresenter.getAllWorkspaceTask(mWorkspace);
            }
        }
    }

    @Override
    public void getAllTask(ArrayList<Task> tasks) {
        arrTasks.clear();
        arrTasks.addAll(tasks);
        customAdaper.notifyDataSetChanged();
    }

    @Override
    public void getAllWorkspace(List<Workspace> workspaces) {

    }

    @Override
    public void loadUserInformation(User user) {

    }

    @Override
    public void successActivity() {

    }

    @Override
    public void onButtonDeleteClickListner(Task task) {
        mPresenter.deleteWorkspaceTask(mWorkspace, task);
        Toast.makeText(getActivity(), "Deleted ",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onButtonEditClickListner(int position) {
        Task task = arrTasks.get(position);
        Intent intent = new Intent(getActivity(), AddNoteActivity.class);
        intent.putExtra("activity", "workspace");
        intent.putExtra("TASK_OBJECT", task);
        intent.putExtra(WORKSPACE_OBJECT, mWorkspace);
        startActivityForResult(intent, REQUEST_CODE_EXAMPLE1);
        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
}
