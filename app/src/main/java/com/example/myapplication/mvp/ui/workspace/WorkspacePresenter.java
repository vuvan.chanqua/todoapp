package com.example.myapplication.mvp.ui.workspace;


import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.myapplication.mvp.data.DataManager;
import com.example.myapplication.mvp.data.firebase.User;
import com.example.myapplication.mvp.data.firebase.Workspace;
import com.example.myapplication.mvp.ui.base.BasePresenter;
import com.example.myapplication.mvp.utils.rx.SchedulerProvider;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

import static com.example.myapplication.mvp.utils.AppConstants.TABLE_USER;
import static com.example.myapplication.mvp.utils.AppConstants.USER_WORKSPACE;

public class WorkspacePresenter<V extends WorkspaceMvpView> extends BasePresenter<V> implements WorkspaceMvpPresenter<V> {

    private static final String TAG = "WorkspacePresenter";
    StorageReference storageRef;
    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private FirebaseFirestore db;
    private List<Workspace> workspaces;

    @Inject
    public WorkspacePresenter(DataManager dataManager,
                              SchedulerProvider schedulerProvider,
                              CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
        this.db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        this.currentUser = mAuth.getCurrentUser();
        this.workspaces = new ArrayList<>();
        this.storageRef = FirebaseStorage.getInstance().getReference();
    }

    @Override
    public void getAllWorkspace(User user) {
        db.collection(TABLE_USER)
                .document(currentUser.getUid())
                .collection(USER_WORKSPACE)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull com.google.android.gms.tasks.Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            workspaces.clear();
                            for (DocumentSnapshot document : task.getResult()) {
                                Workspace workspace = document.toObject(Workspace.class);
                                workspaces.add(workspace);
                                Log.d("WORK_LIST1", workspace.toString());

                            }
                            getMvpView().getAllWorkspace(workspaces);
                        }
                    }
                });
    }

    @Override
    public void addWorkspace(Workspace workspace) {
        if (!workspace.getImg_url().equals("")) {
            createWorkspaceWithImage(workspace);
        } else {
            createWorkspaceWithNoImage(workspace);
        }
    }

    private void createWorkspaceWithNoImage(final Workspace workspace) {
        getMvpView().showLoading();
        final DocumentReference dr = db.collection(USER_WORKSPACE).document();
        final String dcmID = dr.getId();
        workspace.setId(dcmID);
        dr.set(workspace).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    db.collection(TABLE_USER)
                            .document(currentUser.getUid())
                            .get()
                            .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                    if (task.isSuccessful()) {
                                        User user = task.getResult().toObject(User.class);
                                        assert user != null;
                                        dr.collection(TABLE_USER)
                                                .document(currentUser.getUid())
                                                .set(user)
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        getMvpView().onSuccessActivity();
                                                    }
                                                });

                                        db.collection(TABLE_USER)
                                                .document(user.getId())
                                                .collection(USER_WORKSPACE)
                                                .document(workspace.getId())
                                                .set(workspace)
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        getMvpView().onSuccessActivity();
                                                    }
                                                });
                                        getMvpView().hideLoading();
                                    }
                                }
                            });
                }
            }


        });


    }

    private void createWorkspaceWithImage(final Workspace workspace) {
        String img_profile = Long.toString(System.currentTimeMillis());
        final StorageReference profile_img = storageRef.child("workspace_images").child(img_profile + ".png");
        profile_img.putFile(Uri.parse(workspace.getImg_url())).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()) {
                    profile_img.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            DocumentReference dr = db.collection(USER_WORKSPACE).document();
                            workspace.setId(dr.getId());
                            workspace.setImg_url(uri.toString());
                            dr.set(workspace).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    getMvpView().onSuccessActivity();
                                }
                            });
                            db.collection(TABLE_USER)
                                    .document(currentUser.getUid())
                                    .collection(USER_WORKSPACE)
                                    .document(workspace.getId())
                                    .set(workspace)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {

                                        }
                                    });
                        }
                    });
                }
            }
        });
    }

    @Override
    public void updateWokspace(final Workspace workspace) {
        db.collection(USER_WORKSPACE)
                .document(workspace.getId())
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            assert document != null;
                            String img_url = document.getString("img_url");
                            assert img_url != null;
                            Log.d(("IMG_TAG"), workspace.getImg_url());
                            Log.d(("IMG_TAG1"), img_url);
                            if (img_url.equals(workspace.getImg_url())) {
                                updateWorkspaceWithNoImage(workspace);
                            } else {
                                updateWorkspaceWithImage(workspace);
                            }
                        }
                    }
                });
    }

    private void updateWorkspaceWithNoImage(final Workspace workspace) {
        getMvpView().showLoading();
        final Map<String, Object> map = new HashMap<>();
        map.put("name", workspace.getName());
        db.collection(USER_WORKSPACE)
                .document(workspace.getId())
                .update(map)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            db.collection(USER_WORKSPACE)
                                    .document(workspace.getId())
                                    .collection(TABLE_USER)
                                    .get()
                                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                            if (task.isSuccessful()) {
                                                for (DocumentSnapshot documentSnapshot : task.getResult()) {
                                                    String userID = documentSnapshot.getId();
                                                    db.collection(TABLE_USER)
                                                            .document(userID)
                                                            .collection(USER_WORKSPACE)
                                                            .document(workspace.getId())
                                                            .update(map)
                                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                    getMvpView().hideLoading();
                                                                    getMvpView().onSuccessActivity();
                                                                }
                                                            });
                                                }
                                            }
                                        }
                                    });
                        }

                    }
                });

    }

    private void updateWorkspaceWithImage(final Workspace workspace) {
        getMvpView().showLoading();
        String img_profile = Long.toString(System.currentTimeMillis());
        final StorageReference profile_img = storageRef.child("workspace_images").child(img_profile + ".png");
        profile_img.putFile(Uri.parse(workspace.getImg_url())).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()) {
                    profile_img.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            final Map<String, Object> mapObj = new HashMap<>();
                            mapObj.put("img_url", uri.toString());
                            db.collection(USER_WORKSPACE)
                                    .document(workspace.getId())
                                    .update(mapObj)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            db.collection(TABLE_USER)
                                                    .document(currentUser.getUid())
                                                    .collection(USER_WORKSPACE)
                                                    .document(workspace.getId())
                                                    .update(mapObj)
                                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            getMvpView().onSuccessActivity();
                                                        }
                                                    });
                                            getMvpView().hideLoading();
                                            updateWorkspaceWithNoImage(workspace);
                                        }
                                    });
                        }
                    });
                }
            }
        });
    }

    @Override
    public void deleteWorkspace(final Workspace workspace) {
        getMvpView().showLoading();


        Log.d(("DELETE_TAG1"), workspace.getId());
        db.collection(USER_WORKSPACE)
                .document(workspace.getId())
                .collection(TABLE_USER)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (DocumentSnapshot documentSnapshot : task.getResult()) {
                                String userID = documentSnapshot.getId();
                                Log.d(("DELETE_TAG1.1"), userID);
                                db.collection(TABLE_USER)
                                        .document(userID)
                                        .collection(USER_WORKSPACE)
                                        .document(workspace.getId())
                                        .delete()
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {

                                            }
                                        });
                            }

                            db.collection(USER_WORKSPACE)
                                    .document(workspace.getId())
                                    .delete()
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                getMvpView().onSuccessActivity();
                                            }
                                            getMvpView().hideLoading();
                                        }
                                    });
                        }
                    }
                });
    }

    @Override
    public void exitWorkspace(final Workspace workspace) {
        db.collection(TABLE_USER)
                .document(currentUser.getUid())
                .collection(USER_WORKSPACE)
                .document(workspace.getId())
                .delete()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            db.collection(USER_WORKSPACE)
                                    .document(workspace.getId())
                                    .collection(TABLE_USER)
                                    .document(currentUser.getUid())
                                    .delete()
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            getMvpView().onSuccessActivity();
                                        }
                                    });
                        }
                    }
                });
    }

    @Override
    public void addUserWorkspace(final Workspace workspace, final List<User> users) {
        db.collection(USER_WORKSPACE)
                .document(workspace.getId())
                .collection(TABLE_USER)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (DocumentSnapshot document : task.getResult()) {
                                document.getReference().delete();
                            }

                            for (User user : users) {
                                DocumentReference documentReference = db.collection(USER_WORKSPACE)
                                        .document(workspace.getId())
                                        .collection(TABLE_USER)
                                        .document(user.getId());
                                documentReference
                                        .set(user)
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {

                                            }
                                        });

                                db.collection(TABLE_USER)
                                        .document(user.getId())
                                        .collection(USER_WORKSPACE)
                                        .document(workspace.getId())
                                        .set(workspace)
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {

                                            }
                                        });
                            }
                            getMvpView().onSuccessActivity();
                        }
                    }
                });
    }

    @Override
    public void getListUserByWorkspace(final Workspace workspace) {
        final List<User> allUsers = new ArrayList<>();
        db.collection(TABLE_USER)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (DocumentSnapshot document : task.getResult()) {
                                User user = document.toObject(User.class);
                                allUsers.add(user);
                                Log.d("USER_LIST1", user.toString());
                            }

                            final List<User> users = new ArrayList<>();
                            db.collection(USER_WORKSPACE)
                                    .document(workspace.getId())
                                    .collection(TABLE_USER)
                                    .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                    if (task.isSuccessful()) {
                                        for (DocumentSnapshot document : task.getResult()) {
                                            users.add(document.toObject(User.class));
                                        }
                                        getMvpView().listUserChose(allUsers, users);
                                    }
                                }
                            });
                        }
                    }
                });

    }

    @Override
    public void getListUser() {
        final ArrayList<User> users = new ArrayList<>();
        db.collection(TABLE_USER)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (DocumentSnapshot document : (task.getResult())) {
                                User user = document.toObject(User.class);
                                users.add(user);
                            }
                        }
                        getMvpView().listUser(users);
                    }
                });
    }
}