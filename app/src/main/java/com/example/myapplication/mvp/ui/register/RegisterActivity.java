package com.example.myapplication.mvp.ui.register;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;

import com.example.myapplication.mvp.R;
import com.example.myapplication.mvp.ui.base.BaseActivity;
import com.example.myapplication.mvp.ui.login.LoginActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.myapplication.mvp.utils.AppConstants.EMAIL_PATTERN;

@SuppressLint("Registered")
public class RegisterActivity extends BaseActivity implements RegisterMvpView {

    public static final String EXTRA_EMAIL = "EXTRA_EMAIL";
    public static final String EXTRA_PASSWORD = "EXTRA_PASSWORD";

    @Inject
    RegisterPresenter<RegisterMvpView> mPresenter;

    @BindView(R.id.usnText1)
    EditText usnText;

    @BindView(R.id.pwdText1)
    EditText pwdText;

    @BindView(R.id.nameText)
    EditText nameText;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, RegisterActivity.class);
    }

    @OnClick(R.id.backBtn)
    public void setOnBackBtn() {
        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

    }

    @OnClick(R.id.btnSignup1)
    public void setOnSignUp() {
        if (TextUtils.isEmpty(usnText.getText().toString())) {
            usnText.setError("Cannot empty");
        }
        if (TextUtils.isEmpty(pwdText.getText().toString())) {
            pwdText.setError("Cannot empty");
        }
        if (TextUtils.isEmpty(pwdText.getText().toString())) {
            nameText = usnText;
        }
        if (!TextUtils.isEmpty(usnText.getText().toString()) && !TextUtils.isEmpty(pwdText.getText().toString())) {
            if (usnText.getText().toString().matches(EMAIL_PATTERN)) {

            } else {
                usnText.setError("wrong format");
            }
        }
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(RegisterActivity.this);
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {

    }

    @Override
    public void registerSuccess() {
        final Intent data = new Intent();
        data.putExtra(EXTRA_EMAIL, usnText.getText().toString());
        data.putExtra(EXTRA_PASSWORD, pwdText.getText().toString());
        setResult(Activity.RESULT_OK, data);
        finish();

    }

    @Override
    public void registerFailed() {
        showMessage("Register failed");
    }
}