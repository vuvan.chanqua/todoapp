package com.example.myapplication.mvp.ui.profile;

import com.example.myapplication.mvp.ui.base.MvpPresenter;

public interface ProfileMvpPresenter<V extends ProfileMvpView> extends MvpPresenter<V> {
    void changePassword(String oldPwd, String newPwd);

    void saveInformation(String usn, String name, String imge_uri, String date_birth
            , String favorite, String address, String work, int type);

    void getUserInformation();
}