

package com.example.myapplication.mvp.utils;

/**
 * Created by amitshekhar on 08/01/17.
 */

public final class AppConstants {

    public static final String STATUS_CODE_SUCCESS = "success";
    public static final String STATUS_CODE_FAILED = "failed";

    public static final int API_STATUS_CODE_LOCAL_ERROR = 0;

    public static final String DB_NAME = "mindorks_mvp.db";
    public static final String PREF_NAME = "mindorks_pref";

    public static final long NULL_INDEX = -1L;

    public static final String SEED_DATABASE_OPTIONS = "seed/options.json";
    public static final String SEED_DATABASE_QUESTIONS = "seed/questions.json";

    public static final String TIMESTAMP_FORMAT = "yyyyMMdd_HHmmss";


    public static final int REQUEST_CODE_EXAMPLE = 0x9345;
    public static final int REQUEST_CODE_EXAMPLE1 = 0x9346;
    public static final String CURRENT_USER = "CURRENT_USER";
    public static final String TABLE_COLLECTION = "Tasks";
    public static final String TABLE_USER = "Users";
    public static final String USER_WORKSPACE = "Workspaces";
    public static final String TASK_FONT_SIZE = "font_size";
    public static final String EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";



    private AppConstants() {
        // This utility class is not publicly instantiable
    }
}
