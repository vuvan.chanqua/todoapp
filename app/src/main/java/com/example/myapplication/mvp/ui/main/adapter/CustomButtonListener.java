package com.example.myapplication.mvp.ui.main.adapter;


import com.example.myapplication.mvp.data.firebase.Task;

public interface CustomButtonListener {
    void onButtonDeleteClickListner(Task task);
    void onButtonEditClickListner(int position);
}
