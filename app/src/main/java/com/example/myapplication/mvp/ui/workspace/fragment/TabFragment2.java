package com.example.myapplication.mvp.ui.workspace.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.myapplication.mvp.R;
import com.example.myapplication.mvp.data.firebase.User;
import com.example.myapplication.mvp.data.firebase.Workspace;
import com.example.myapplication.mvp.ui.base.BaseFragment;
import com.example.myapplication.mvp.ui.workspace.WorkspaceActivity;
import com.example.myapplication.mvp.ui.workspace.WorkspaceMvpView;
import com.example.myapplication.mvp.ui.workspace.WorkspacePresenter;
import com.example.myapplication.mvp.ui.workspace.adapter.workspace_user.WorkspaceUserAdapter;
import com.example.myapplication.mvp.ui.workspace.adapter.workspace_user.WorkspaceUserListener;
import com.example.myapplication.mvp.utils.SharedPrefs;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class TabFragment2 extends BaseFragment implements WorkspaceMvpView, WorkspaceUserListener {

    @Inject
    WorkspacePresenter<WorkspaceMvpView> mPresenter;

    private static final String WORKSPACE_OBJECT = "WORKSPACE_OBJECT";
    private RecyclerView recyclerView;
    ArrayAdapter<User> arrayAdapter;
    Spinner spinner;
    private WorkspaceUserAdapter userWorkspaceAdapter;
    private List<User> userList, userChosen;
    private Button button;
    SharedPreferences mPrefs;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        setUp(view);
    }

    @Override
    protected void setUp(View view) {
        button = (Button) view.findViewById(R.id.addwpUsertbn);
        recyclerView = (RecyclerView) view.findViewById(R.id.wpUserRecyclerView);
        spinner = (Spinner) view.findViewById(R.id.spinner2);

        getActivityComponent().inject(this);
        setUnBinder( ButterKnife.bind(this, view));
        mPresenter.onAttach(TabFragment2.this);

        userList = new ArrayList<>();
        userChosen = new ArrayList<>();

        final Intent data = getActivity().getIntent();
        final Workspace workspace = (Workspace) data.getSerializableExtra(WORKSPACE_OBJECT);

        if (workspace != null) {
            mPresenter.getListUserByWorkspace(workspace);
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (workspace != null && userChosen != null) {
                    mPresenter.addUserWorkspace(workspace, userList);
                }
            }
        });
        mPrefs = getActivity().getPreferences(Context.MODE_PRIVATE);
        mPresenter.getListUser();
    }

    private void setupSpinner() {
        userChosen.add(0, new User());
        arrayAdapter = new ArrayAdapter<User> (getActivity(), android.R.layout.simple_spinner_item, userChosen);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                User user = (User) parent.getSelectedItem();
                if (user.getId() != null) {
                    userList.add(user);
                }
                userChosen.remove(position);
                recyclerView.setAdapter(userWorkspaceAdapter);
//                Toast.makeText(getActivity(), user.getName(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @SuppressLint("WrongConstant")
    public void setupRecycleView1() {
        userWorkspaceAdapter = new WorkspaceUserAdapter(userList);
        userWorkspaceAdapter.setWorkspacesUserButtonListner(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(userWorkspaceAdapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tab_fragment_2, container, false);
    }

    @Override
    public void getAllWorkspace(List<Workspace> workspaces) {

    }

    @Override
    public void onSuccessActivity() {
        Toast.makeText(getActivity(), "Submitted", Toast.LENGTH_SHORT).show();
//        backActivity();
    }

    @Override
    public void listUser(List<User> users) {
        userChosen.clear();
        userChosen.addAll(users);
        setupSpinner();
        setupRecycleView1();
    }

    @Override
    public void listUserChose(List<User> allUsers, List<User> users) {
        userList.clear();
        userList.addAll(users);
        for (int i=0 ;i<users.size(); i++) {
            for (int j = 0; j< allUsers.size(); j++){
                if (users.get(i).getId().equals(allUsers.get(j).getId())){
                    allUsers.remove(j);
                }
            }
        }
        Log.d("WORKSPACE_USER_TAG", users.toString());
        Log.d("WORKSPACE_USER_TAG", allUsers.toString());

        userChosen.clear();
        userChosen.addAll(allUsers);
        recyclerView.setAdapter(userWorkspaceAdapter);
        setupSpinner();
    }


    @Override
    public void onSelectUser(int position) {
        Toast.makeText(getActivity(), "Selected: " + userList.get(position).getName(), Toast.LENGTH_SHORT).show();
        userChosen.add(userList.get(position));
        userList.remove(userList.get(position));
        setupSpinner();
        setupRecycleView1();
    }

    @Override
    public void onUnSelectUser(int position) {
        Toast.makeText(getActivity(), "UnSelected: " + userList.get(position).getName(), Toast.LENGTH_SHORT).show();
    }

    public void backActivity() {
        Intent intent = new Intent(getActivity(), WorkspaceActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
}
