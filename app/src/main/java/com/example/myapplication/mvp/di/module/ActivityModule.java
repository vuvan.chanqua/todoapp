

package com.example.myapplication.mvp.di.module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import com.example.myapplication.mvp.di.ActivityContext;
import com.example.myapplication.mvp.di.PerActivity;
import com.example.myapplication.mvp.ui.add_note.AddNoteMvpPresenter;
import com.example.myapplication.mvp.ui.add_note.AddNoteMvpView;
import com.example.myapplication.mvp.ui.login.LoginMvpPresenter;
import com.example.myapplication.mvp.ui.login.LoginMvpView;
import com.example.myapplication.mvp.ui.login.LoginPresenter;
import com.example.myapplication.mvp.ui.main.MainMvpPresenter;
import com.example.myapplication.mvp.ui.main.MainMvpView;
import com.example.myapplication.mvp.ui.main.MainPresenter;
import com.example.myapplication.mvp.ui.profile.ProfileMvpPresenter;
import com.example.myapplication.mvp.ui.profile.ProfileMvpView;
import com.example.myapplication.mvp.ui.register.RegisterMvpPresenter;
import com.example.myapplication.mvp.ui.register.RegisterMvpView;
import com.example.myapplication.mvp.ui.splash.SplashMvpPresenter;
import com.example.myapplication.mvp.ui.splash.SplashMvpView;
import com.example.myapplication.mvp.ui.splash.SplashPresenter;
import com.example.myapplication.mvp.ui.workspace.WorkspaceMvpPresenter;
import com.example.myapplication.mvp.ui.workspace.WorkspaceMvpView;
import com.example.myapplication.mvp.utils.rx.AppSchedulerProvider;
import com.example.myapplication.mvp.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;



@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    AddNoteMvpPresenter<AddNoteMvpView> provideAddNotePresenter(
            AddNoteMvpPresenter<AddNoteMvpView> presenter) {
        return presenter;
    }

    @Provides
    ProfileMvpPresenter<ProfileMvpView> provideProfilePresenter(
            ProfileMvpPresenter<ProfileMvpView> presenter) {
        return presenter;
    }

    @Provides
    WorkspaceMvpPresenter<WorkspaceMvpView> provideWorkspacePresenter(
            WorkspaceMvpPresenter<WorkspaceMvpView> presenter) {
        return presenter;
    }

    @Provides
    RegisterMvpPresenter<RegisterMvpView> provideRegisterPresenter(
            RegisterMvpPresenter<RegisterMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    LoginMvpPresenter<LoginMvpView> provideLoginPresenter(
            LoginPresenter<LoginMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    SplashMvpPresenter<SplashMvpView> provideSplashPresenter(
            SplashPresenter<SplashMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    MainMvpPresenter<MainMvpView> provideMainPresenter(
            MainPresenter<MainMvpView> presenter) {
        return presenter;
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(AppCompatActivity activity) {
        return new LinearLayoutManager(activity);
    }
}
