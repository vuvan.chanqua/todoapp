package com.example.myapplication.mvp.ui.add_note;


import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.myapplication.mvp.data.DataManager;
import com.example.myapplication.mvp.data.firebase.Task;
import com.example.myapplication.mvp.data.firebase.Workspace;
import com.example.myapplication.mvp.ui.base.BasePresenter;
import com.example.myapplication.mvp.utils.rx.SchedulerProvider;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

import static com.example.myapplication.mvp.utils.AppConstants.TABLE_COLLECTION;
import static com.example.myapplication.mvp.utils.AppConstants.USER_WORKSPACE;

public class AddNotePresenter<V extends AddNoteMvpView> extends BasePresenter<V> implements AddNoteMvpPresenter<V> {

    private static final String TAG = "AddNotePresenter";
    private FirebaseFirestore db;
    private StorageReference storageRef;
    private FirebaseAuth mAuth;


    @Inject
    public AddNotePresenter(DataManager dataManager,
                            SchedulerProvider schedulerProvider,
                            CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
        this.mAuth = FirebaseAuth.getInstance();
        this.db = FirebaseFirestore.getInstance();
        this.storageRef = FirebaseStorage.getInstance().getReference();
    }

    @Override
    public void createTask(Task note) {
        getMvpView().showLoading();
        Workspace workspace = new Workspace();
        workspace.setId("0");
        Map<String, Object> objectTask = new HashMap<>();
        objectTask.put("id", note.getId());
        objectTask.put("text", note.getText());
        objectTask.put("type", 0);
        objectTask.put("user_id", note.getUser_id());
        objectTask.put("img_url_1", note.getImg_url_1());
        objectTask.put("img_url_2", note.getImg_url_2());
        objectTask.put("img_url_3", note.getImg_url_3());
        objectTask.put("color", note.getColor());
        objectTask.put("font_size", note.getFont_size());

        db.collection(TABLE_COLLECTION)
                .document(note.getId())
                .set(objectTask)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        Log.d("DB_TAG", "create tag");
                    }
                });
        uploadImage(workspace, note);
    }

    @Override
    public void updateTask(Task note) {
        getMvpView().showLoading();
        Workspace workspace = new Workspace();
        workspace.setId("0");
        Map<String, Object> objectTask = new HashMap<>();
        objectTask.put("id", note.getId());
        objectTask.put("text", note.getText());
        objectTask.put("user_id", note.getUser_id());
        objectTask.put("color", note.getColor());
        objectTask.put("font_size", note.getFont_size());

        db.collection(TABLE_COLLECTION)
                .document(note.getId())
                .update(objectTask).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d("DB_TAG", "update tag");
            }
        });
        uploadImage(workspace, note);
    }

    @Override
    public void createWorkspaceTask(Workspace workspace, Task note) {
        Map<String, Object> objectTask = new HashMap<>();
        objectTask.put("id", note.getId());
        objectTask.put("text", note.getText());
        objectTask.put("type", 1);
        objectTask.put("user_id", note.getUser_id());
        objectTask.put("img_url_1", note.getImg_url_1());
        objectTask.put("img_url_2", note.getImg_url_2());
        objectTask.put("img_url_3", note.getImg_url_3());
        objectTask.put("color", note.getColor());
        objectTask.put("font_size", note.getFont_size());

        db.collection(USER_WORKSPACE)
                .document(workspace.getId())
                .collection(TABLE_COLLECTION)
                .document(note.getId())
                .set(objectTask)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull com.google.android.gms.tasks.Task<Void> task) {
                        if (task.isSuccessful()) {
//                            getMvpView().createSuccess();
                        }
                    }
                });
        uploadImage(workspace, note);
    }

    @Override
    public void updateWorkspaceTask(Workspace workspace, Task note) {
        getMvpView().showLoading();
        Map<String, Object> objectTask = new HashMap<>();
        objectTask.put("id", note.getId());
        objectTask.put("text", note.getText());
        objectTask.put("user_id", note.getUser_id());
        objectTask.put("color", note.getColor());
        objectTask.put("font_size", note.getFont_size());

        db.collection(USER_WORKSPACE)
                .document(workspace.getId())
                .collection(TABLE_COLLECTION)
                .document(note.getId())
                .update(objectTask)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("DB_TAG", "update tag");
                    }
                });
        uploadImage(workspace, note);
    }

    private void uploadImage(final Workspace workspace, final Task note) {
        final List<String> noteArray = new ArrayList<>();
        if (!note.getImg_url_1().equals("")) {
            noteArray.add(note.getImg_url_1());
        }
        if (!note.getImg_url_2().equals("")) {
            noteArray.add(note.getImg_url_2());
        }
        if (!note.getImg_url_3().equals("")) {
            noteArray.add(note.getImg_url_3());
        }
        if (noteArray.size() == 0) {
            getMvpView().hideLoading();
            getMvpView().createSuccess();
        }
        Log.d("UPLOAD_TAG", String.valueOf(noteArray.size()));
        if (noteArray.size() >= 1) {
            String img_name = Long.toString(System.currentTimeMillis());
            final StorageReference task_img = storageRef.child("task_images").child(img_name + ".png");
            task_img.putFile(Uri.parse(note.getImg_url_1())).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull com.google.android.gms.tasks.Task<UploadTask.TaskSnapshot> task) {
                    if (task.isSuccessful()) {
                        task_img.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                note.setImg_url_1(uri.toString());
                                if (workspace.getId().equals("0")) {
                                    updateTaskImage(note);
                                } else {
                                    updateWorkspaceTask(workspace, note);
                                }

                                if (noteArray.size() == 1) {
                                    getMvpView().createSuccess();
                                }
                            }
                        });
                    }
                }
            });
        }
        if (noteArray.size() >= 2) {
            String img_name1 = Long.toString(System.currentTimeMillis());
            final StorageReference task_img1 = storageRef.child("task_images").child(img_name1 + ".png");
            task_img1.putFile(Uri.parse(note.getImg_url_2())).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull com.google.android.gms.tasks.Task<UploadTask.TaskSnapshot> task) {
                    if (task.isSuccessful()) {
                        task_img1.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                note.setImg_url_2(uri.toString());
                                if (workspace.getId().equals("0")) {
                                    updateTaskImage(note);
                                } else {
                                    updateWorkspaceTask(workspace, note);
                                }
                                if (noteArray.size() == 2) {
                                    getMvpView().createSuccess();
                                }
                            }
                        });
                    }
                }
            });
        }
        if (noteArray.size() == 3) {
            String img_name2 = Long.toString(System.currentTimeMillis());
            final StorageReference task_img2 = storageRef.child("task_images").child(img_name2 + ".png");
            task_img2.putFile(Uri.parse(note.getImg_url_3())).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull final com.google.android.gms.tasks.Task<UploadTask.TaskSnapshot> task) {
                    if (task.isSuccessful()) {
                        task_img2.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                note.setImg_url_3(uri.toString());
                                if (workspace.getId().equals("0")) {
                                    updateTaskImage(note);
                                } else {
                                    updateWorkspaceTask(workspace, note);
                                }
                                if (noteArray.size() == 3) {
                                    getMvpView().createSuccess();
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    @Override
    public void updateTaskImage(Task note) {
        Map<String, Object> objectTask = new HashMap<>();
        objectTask.put("img_url_1", note.getImg_url_1());
        objectTask.put("img_url_2", note.getImg_url_2());
        objectTask.put("img_url_3", note.getImg_url_3());

        db.collection(TABLE_COLLECTION)
                .document(note.getId())
                .update(objectTask).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                getMvpView().hideLoading();
                Log.d("DB_TAG", "update tag");
            }
        });
    }
}