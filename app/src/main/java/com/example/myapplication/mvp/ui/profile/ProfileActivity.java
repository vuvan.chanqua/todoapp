package com.example.myapplication.mvp.ui.profile;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.myapplication.mvp.R;
import com.example.myapplication.mvp.data.firebase.User;
import com.example.myapplication.mvp.ui.base.BaseActivity;
import com.example.myapplication.mvp.ui.main.MainActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.myapplication.mvp.utils.AppConstants.EMAIL_PATTERN;


public class ProfileActivity extends BaseActivity implements ProfileMvpView {

    @Inject
    ProfilePresenter<ProfileMvpView> mPresenter;

    @BindView(R.id.imgprfbtn)
    ImageButton imgBtn;

    @BindView(R.id.saveprfBtn)
    Button saveBtn;

    @BindView(R.id.changepwdBtn)
    Button changepwdBtn;

    @BindView(R.id.usnprfText)
    EditText usnText;

    @BindView(R.id.nameprfText)
    EditText nameText;

    @BindView(R.id.datebirthText)
    EditText datebirthText;

    @BindView(R.id.favoriteText)
    EditText favoriteText;

    @BindView(R.id.addressText)
    EditText addressText;

    @BindView(R.id.workText)
    EditText workText;

    @BindView(R.id.roleSpn)
    Spinner roleSpn;

    @BindView(R.id.dateBtn)
    Button dateBtn;


    FirebaseAuth mAuth;
    FirebaseUser currentUser;
    private static final int PICK_IMAGE = 1;

    private String imgUri;
    private int mYear, mMonth, mDay;
    private int userType = 0;
    private List<String> user_spinner = new ArrayList<>();


    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, ProfileActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(ProfileActivity.this);
        setupToolbar();
        setUp();
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setTextSize(22);
        toolbarTitle.setText(getResources().getString(R.string.note));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }


    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        usnText.setText(currentUser.getEmail());
        nameText.setText(currentUser.getDisplayName());
        mPresenter.getUserInformation();
        if (currentUser.getPhotoUrl() != null) {
            Glide.with(getApplicationContext()).load(currentUser.getPhotoUrl()).into(imgBtn);
            Log.d("PROFILE_TAG", currentUser.getPhotoUrl().toString());
        }


        imgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(usnText.getText().toString())) {
                    usnText.setError("Cannot empty");
                }
                if (!TextUtils.isEmpty(usnText.getText().toString())) {
                    if (usnText.getText().toString().matches(EMAIL_PATTERN)) {
                        if (imgUri == null) {
                            imgUri = "";
                        }
                        mPresenter.saveInformation(
                                usnText.getText().toString(),
                                nameText.getText().toString(),
                                imgUri,
                                datebirthText.getText().toString(),
                                favoriteText.getText().toString(),
                                addressText.getText().toString(),
                                workText.getText().toString(),
                                userType);
                    } else {
                        usnText.setError("Wrong format");
                    }
                }

            }
        });


        dateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datepickerdialog = new DatePickerDialog(ProfileActivity.this,
                        AlertDialog.THEME_HOLO_LIGHT, new DatePickerDialog.OnDateSetListener() {

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        datebirthText.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);

                    }
                }, mYear, mMonth, mDay);

                datepickerdialog.show();
            }
        });

        changepwdBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileActivity.this, PasswordActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
        userTypeChange();
    }

    public void userTypeChange() {
        user_spinner.add(getResources().getString(R.string.standard));
        user_spinner.add(getResources().getString(R.string.premium));


        ArrayAdapter<String> adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, user_spinner);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        roleSpn.setAdapter(adapter);
        roleSpn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                String itemSelected = roleSpn.getSelectedItem().toString();
                if (itemSelected.equals(getResources().getString(R.string.standard))) {
                    userType = 0;
                }
                if (itemSelected.equals(getResources().getString(R.string.premium))) {
                    userType = 1;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @Override
    public void loadUserInformation(User userInfo) {
        datebirthText.setText(userInfo.getDate_of_birth());
        favoriteText.setText(userInfo.getFavorite());
        addressText.setText(userInfo.getAddress());
        workText.setText(userInfo.getWork());
        roleSpn.setSelection(userInfo.getType());
    }

    @Override
    public void checkPasswordSuccess() {

    }

    @Override
    public void saveInformationSuccess() {

    }

    @Override
    public void saveInformationFailed() {

    }

    public void selectImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK && data != null) {
            Uri uri = data.getData();
            assert uri != null;
            imgUri = uri.toString();
            Glide.with(ProfileActivity.this).load(uri).into(imgBtn);

        } else {
            Log.d("CODE_REQUEST", "request code" + requestCode);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}