

package com.example.myapplication.mvp.ui.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.mvp.R;
import com.example.myapplication.mvp.data.firebase.Task;
import com.example.myapplication.mvp.data.firebase.User;
import com.example.myapplication.mvp.data.firebase.Workspace;
import com.example.myapplication.mvp.ui.add_note.AddNoteActivity;
import com.example.myapplication.mvp.ui.base.BaseActivity;
import com.example.myapplication.mvp.ui.login.LoginActivity;
import com.example.myapplication.mvp.ui.main.adapter.CustomAdapter;
import com.example.myapplication.mvp.ui.main.adapter.CustomButtonListener;
import com.example.myapplication.mvp.ui.profile.ProfileActivity;
import com.example.myapplication.mvp.ui.workspace.WorkspaceActivity;
import com.example.myapplication.mvp.utils.SharedPrefs;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.myapplication.mvp.utils.AppConstants.CURRENT_USER;
import static com.example.myapplication.mvp.utils.AppConstants.REQUEST_CODE_EXAMPLE;


public class MainActivity extends BaseActivity implements MainMvpView, CustomButtonListener {

    private static final String WORKSPACE_OBJECT = "WORKSPACE_OBJECT";

    @Inject
    MainMvpPresenter<MainMvpView> mPresenter;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.button)
    Button button;

    @BindView(R.id.listItem)
    ListView listView;

    @BindView(R.id.spnWp)
    Spinner spnWp;

    ArrayList<Task> arrTasks = new ArrayList<>();
    CustomAdapter customAdapter;
    ArrayAdapter<Workspace> adapterWorkspace;
    List<Workspace> mWorkspace;
    Workspace currentWorkspace;
    SharedPreferences mPrefs;
    private int positionWorkspace = 0;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));

        mPresenter.onAttach(this);


        mPresenter.getAllWorkspace();
        Log.d("WORKSPACE_POSITION", "Maincreate");
        setUp();
        setupToolbar();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddNoteActivity.class);
                intent.putExtra("activity", "main");
                startActivityForResult(intent, REQUEST_CODE_EXAMPLE);
            }
        });
    }

    @Override
    protected void setUp() {
        customAdapter = new CustomAdapter(this, R.layout.item_note, arrTasks);
        customAdapter.setCustomButtonListner(MainActivity.this);
        listView.setAdapter(customAdapter);

        mWorkspace = new ArrayList<>();
        currentWorkspace = new Workspace();
        processWorkspace();
        mPrefs = getPreferences(MODE_PRIVATE);
        mPresenter.loadUserInformation();
    }

    public void processWorkspace() {
        adapterWorkspace = new ArrayAdapter(this, android.R.layout.simple_spinner_item, mWorkspace);
        adapterWorkspace.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spnWp.setAdapter(adapterWorkspace);
        spnWp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                Workspace workspace = (Workspace) adapterView.getSelectedItem();
                currentWorkspace = workspace;
                mPresenter.changeWorkspace(workspace);
                positionWorkspace = position;
                Log.d("WORKSPACE_POSITION2", mWorkspace.toString());
                Log.d("WORKSPACE_POSITION2", String.valueOf(positionWorkspace));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setTextSize(22);
        toolbarTitle.setText(getResources().getString(R.string.note));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.custom_action_bar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.profile:
                Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;
            case R.id.workspace:
                Intent intent1 = new Intent(MainActivity.this, WorkspaceActivity.class);
                startActivity(intent1);
                finish();
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;
            case R.id.logout:
                mPresenter.logout();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }


    @Override
    public void openLoginActivity() {
        startActivity(LoginActivity.getStartIntent(this));
        finish();
        overridePendingTransition(0, 0);
    }

    @Override
    public void getAllTask(ArrayList<Task> tasks) {
        arrTasks.clear();
        arrTasks.addAll(tasks);
        customAdapter.notifyDataSetChanged();
    }

    @Override
    public void getAllWorkspace(List<Workspace> workspaces) {
        mWorkspace.clear();
        mWorkspace.add(new Workspace("0", "Personal", "", ""));
        mWorkspace.add(new Workspace("1", "All", "", ""));
        mWorkspace.addAll(workspaces);
        spnWp.setAdapter(adapterWorkspace);
    }

    @Override
    public void loadUserInformation(User user) {
        SharedPrefs.getInstance().put(CURRENT_USER, user);
    }


    @Override
    public void onButtonDeleteClickListner(Task task) {
        mPresenter.deleteTask(task);
        Toast.makeText(MainActivity.this, "Deleted ",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onButtonEditClickListner(int position) {
        Task task = arrTasks.get(position);
        Intent intent = new Intent(MainActivity.this, AddNoteActivity.class);
        intent.putExtra("TASK_OBJECT", task);
        intent.putExtra("activity", "main");
        intent.putExtra("position_workspace", positionWorkspace);
        if (task.getType() == 1) {
            intent.putExtra(WORKSPACE_OBJECT, currentWorkspace);
        }
        startActivityForResult(intent, REQUEST_CODE_EXAMPLE);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_EXAMPLE) {
            if (resultCode == Activity.RESULT_OK) {
                Intent i = getIntent();
                mPresenter.getAllWorkspace();
                positionWorkspace = i.getIntExtra("position_workspace", 0);
                Log.d("WORKSPACE_POSITION1", String.valueOf(positionWorkspace));
                spnWp.setSelection(positionWorkspace);
            }
        }
    }
}
