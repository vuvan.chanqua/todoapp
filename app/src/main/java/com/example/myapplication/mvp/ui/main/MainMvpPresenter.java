

package com.example.myapplication.mvp.ui.main;


import com.example.myapplication.mvp.data.firebase.Task;
import com.example.myapplication.mvp.data.firebase.Workspace;
import com.example.myapplication.mvp.di.PerActivity;
import com.example.myapplication.mvp.ui.base.MvpPresenter;


@PerActivity
public interface MainMvpPresenter<V extends MainMvpView> extends MvpPresenter<V> {
    void logout();

    void getAllTask();

    void getAllWorkspace();

    void changeWorkspace(Workspace workspace);

    void deleteTask(Task task);

    void loadUserInformation();

    void getAllWorkspaceTask(Workspace workspace);

    void deleteWorkspaceTask(Workspace workspace, Task task);
}
