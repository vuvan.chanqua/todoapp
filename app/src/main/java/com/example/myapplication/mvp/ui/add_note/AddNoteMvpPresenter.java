package com.example.myapplication.mvp.ui.add_note;

import com.example.myapplication.mvp.data.firebase.Task;
import com.example.myapplication.mvp.data.firebase.Workspace;
import com.example.myapplication.mvp.ui.base.MvpPresenter;

public interface AddNoteMvpPresenter<V extends AddNoteMvpView> extends MvpPresenter<V> {
    void createTask(Task note);

    void updateTaskImage(Task note);

    void updateTask(Task note);

    void createWorkspaceTask(Workspace workspace, Task note);

    void updateWorkspaceTask(Workspace workspace, Task note);
}