package com.example.myapplication.mvp.ui.splash;


import com.example.myapplication.mvp.di.PerActivity;
import com.example.myapplication.mvp.ui.base.MvpPresenter;


@PerActivity
public interface SplashMvpPresenter<V extends SplashMvpView> extends MvpPresenter<V> {

}
