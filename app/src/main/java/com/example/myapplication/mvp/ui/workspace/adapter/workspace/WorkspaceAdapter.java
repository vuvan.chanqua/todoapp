package com.example.myapplication.mvp.ui.workspace.adapter.workspace;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;;
import com.example.myapplication.mvp.R;
import com.example.myapplication.mvp.data.firebase.Workspace;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;

public class WorkspaceAdapter extends RecyclerView.Adapter<WorkspaceAdapter.WorkspaceViewHolder> {
    
    private List<Workspace> workspaces;
    private WorkspaceListener workspaceListener;
    private FirebaseUser currentUser;
    private FirebaseAuth mAuth;

    public void setWorkspacesButtonListner(WorkspaceListener workspaceListener) {
        this.workspaceListener = workspaceListener;
    }
    public WorkspaceAdapter( List<Workspace> workspaces){
        this.workspaces = workspaces;
    }
    
    @NonNull
    @Override
    public WorkspaceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_workspace, parent, false);
        return new WorkspaceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull WorkspaceViewHolder holder, final int position) {
        final Workspace workspace =  workspaces.get(position);
        holder.textView.setText(workspace.getName());
            Glide.with(holder.itemView.getContext()).load(workspace.getImg_url()).into(holder.avatar);
        currentUser = FirebaseAuth.getInstance().getCurrentUser();

        if (!currentUser.getUid().equals(workspace.getUser_id())){
            holder.joinBtn.setVisibility(View.GONE);
            holder.exitBtn.setVisibility(View.GONE);
        }
        if (currentUser.getUid().equals(workspace.getUser_id())){
            holder.outBtn.setVisibility(View.GONE);
        }
        holder.joinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (workspaceListener != null) {
                    workspaceListener.onButtonEditClickListner(position);
                }
            }
        });

        holder.exitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (workspaceListener != null) {
                    workspaceListener.onButtonDeleteClickListner(workspace);
                }
            }
        });

        holder.outBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (workspaceListener != null) {
                    workspaceListener.onButtonOutClickListner(workspace);
                }
            }
        });
    }
    
    @Override
    public int getItemCount() {
        return workspaces.size();
    }

    public class WorkspaceViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        ImageButton avatar;
        ImageView joinBtn, exitBtn, outBtn;
        public WorkspaceViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.wpText);
            avatar = (ImageButton) itemView.findViewById(R.id.wpAvatar);
            joinBtn = (ImageView) itemView.findViewById(R.id.join_wp_btn);
            exitBtn = (ImageView) itemView.findViewById(R.id.exit_wp_btn);
            outBtn = (ImageView) itemView.findViewById(R.id.out_wp_btn);
        }
    }
}
