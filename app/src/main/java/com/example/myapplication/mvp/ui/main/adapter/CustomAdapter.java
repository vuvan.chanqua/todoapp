package com.example.myapplication.mvp.ui.main.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapplication.mvp.R;
import com.example.myapplication.mvp.data.firebase.Task;
import com.example.myapplication.mvp.data.firebase.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

import static com.example.myapplication.mvp.utils.AppConstants.TABLE_USER;

public class CustomAdapter extends ArrayAdapter<Task> {
    private Context mContext;
    private List<Task> arrTask;
    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private CustomButtonListener customListner;

    public void setCustomButtonListner(CustomButtonListener customListner) {
        this.customListner = customListner;
    }

    public CustomAdapter(Context context, int resource, ArrayList<Task> arrTask) {
        super(context, resource, arrTask);
        this.mContext = context;
        this.arrTask = arrTask;
        this.mAuth = FirebaseAuth.getInstance();
        this.currentUser = mAuth.getCurrentUser();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        LayoutInflater mInflater = (LayoutInflater) mContext
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_note, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvName = (TextView) convertView.findViewById(R.id.tvName);
            viewHolder.tvAuthor = (TextView) convertView.findViewById(R.id.tvAuthor);
            viewHolder.tvColor = (ImageButton) convertView.findViewById(R.id.tvColor);
            viewHolder.delete_btn = (ImageView) convertView.findViewById(R.id.delete_btn);
            viewHolder.edit_btn = (ImageView) convertView.findViewById(R.id.edit_btn);
            viewHolder.tvFont = (TextView) convertView.findViewById(R.id.tvFont);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final Task note = arrTask.get(position);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            viewHolder.tvName.setText(Html.fromHtml(note.getText(), Html.FROM_HTML_MODE_COMPACT));
        } else {
            viewHolder.tvName.setText(Html.fromHtml(note.getText()));
        }

        viewHolder.tvName.setTextSize(note.getFont_size());
        if (note.getColor() != 0) {
            viewHolder.tvColor.setBackgroundColor(note.getColor());

        } else {
            viewHolder.tvColor.setBackgroundColor(mContext.getResources().getColor(R.color.white));
        }
        viewHolder.tvFont.setText("size: " + note.getFont_size());

        final int pst = position;

        if (!currentUser.getUid().equals(note.getUser_id())) {
            viewHolder.delete_btn.setVisibility(View.GONE);
            viewHolder.edit_btn.setVisibility(View.GONE);
        }
        if (note.getType() == 1) {
//            viewHolder.delete_btn.setVisibility(View.GONE);
//            viewHolder.edit_btn.setVisibility(View.GONE);
            viewHolder.tvAuthor.setText(R.string.workspace_note);
        } else {
            db = FirebaseFirestore.getInstance();
            db.collection(TABLE_USER)
                    .document(note.getUser_id())
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull com.google.android.gms.tasks.Task<DocumentSnapshot> task) {
                            if (task.isSuccessful()) {
                                User user = task.getResult().toObject(User.class);
                                assert user != null;
                                viewHolder.tvAuthor.setText(user.getEmail());
                            }
                        }
                    });
        }
        viewHolder.delete_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (customListner != null) {
                    customListner.onButtonDeleteClickListner(note);
                }

            }
        });

        viewHolder.edit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (customListner != null) {
                    customListner.onButtonEditClickListner(pst);
                }
            }
        });

        return convertView;
    }

    public class ViewHolder {
        TextView tvName, tvFont, tvAuthor;
        ImageButton tvColor;
        ImageView delete_btn, edit_btn;
    }
}
