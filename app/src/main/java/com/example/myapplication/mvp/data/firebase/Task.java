package com.example.myapplication.mvp.data.firebase;

import java.io.Serializable;

public class Task implements Serializable {
    private String id;
    private String user_id;
    private String text;
    private int type;
    private String img_url_1;
    private String img_url_2;
    private String img_url_3;
    private int color;
    private float font_size;

    public Task(String id, String user_id, String text, String img_url_1, String img_url_2, String img_url_3, int color, float font_size) {
        this.id = id;
        this.user_id = user_id;
        this.text = text;
        this.img_url_1 = img_url_1;
        this.img_url_2 = img_url_2;
        this.img_url_3 = img_url_3;
        this.color = color;
        this.font_size = font_size;
    }

    public Task() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public float getFont_size() {
        return font_size;
    }

    public void setFont_size(float font_size) {
        this.font_size = font_size;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getImg_url_1() {
        return img_url_1;
    }

    public void setImg_url_1(String img_url_1) {
        this.img_url_1 = img_url_1;
    }

    public String getImg_url_2() {
        return img_url_2;
    }

    public void setImg_url_2(String img_url_2) {
        this.img_url_2 = img_url_2;
    }

    public String getImg_url_3() {
        return img_url_3;
    }

    public void setImg_url_3(String img_url_3) {
        this.img_url_3 = img_url_3;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id='" + id + '\'' +
                ", user_id='" + user_id + '\'' +
                ", text='" + text + '\'' +
                ", img_url_1='" + img_url_1 + '\'' +
                ", img_url_2='" + img_url_2 + '\'' +
                ", img_url_3='" + img_url_3 + '\'' +
                ", color=" + color +
                ", font_size=" + font_size +
                '}';
    }
}
