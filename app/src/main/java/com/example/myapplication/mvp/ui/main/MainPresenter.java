

package com.example.myapplication.mvp.ui.main;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.myapplication.mvp.data.DataManager;
import com.example.myapplication.mvp.data.firebase.Task;
import com.example.myapplication.mvp.data.firebase.User;
import com.example.myapplication.mvp.data.firebase.Workspace;
import com.example.myapplication.mvp.ui.base.BasePresenter;
import com.example.myapplication.mvp.utils.rx.SchedulerProvider;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

import static com.example.myapplication.mvp.utils.AppConstants.TABLE_COLLECTION;
import static com.example.myapplication.mvp.utils.AppConstants.TABLE_USER;
import static com.example.myapplication.mvp.utils.AppConstants.USER_WORKSPACE;


public class MainPresenter<V extends MainMvpView> extends BasePresenter<V>
        implements MainMvpPresenter<V> {

    private static final String TAG = "MainPresenter";
    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private FirebaseFirestore db;
    private final ArrayList<Task> arrTasks = new ArrayList<>();

    @Inject
    public MainPresenter(DataManager dataManager,
                         SchedulerProvider schedulerProvider,
                         CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
        this.mAuth = FirebaseAuth.getInstance();
        this.db = FirebaseFirestore.getInstance();
        this.currentUser = mAuth.getCurrentUser();
    }

    @Override
    public void logout() {
        mAuth.signOut();
        getMvpView().openLoginActivity();
    }

    @Override
    public void getAllTask() {
        getMvpView().showLoading();
        db.collection(TABLE_COLLECTION)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull com.google.android.gms.tasks.Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            getMvpView().hideLoading();
                            arrTasks.clear();
                            for (DocumentSnapshot document : task.getResult()) {
                                Task note = document.toObject(Task.class);
                                assert note != null;
                                if (currentUser.getUid().equals(note.getUser_id())) {
                                    arrTasks.add(note);
                                }
                                Log.d("DB_TAG", note.getText());
                            }
                            getMvpView().getAllTask(arrTasks);
                        }
                    }
                });
    }

    @Override
    public void getAllWorkspace() {
        final List<Workspace> workspaces = new ArrayList<>();
        db.collection(TABLE_USER)
                .document(currentUser.getUid())
                .collection(USER_WORKSPACE)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull com.google.android.gms.tasks.Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (DocumentSnapshot documentSnapshot : task.getResult()) {
                                Workspace workspace = documentSnapshot.toObject(Workspace.class);
                                workspaces.add(workspace);
                            }
                            getMvpView().getAllWorkspace(workspaces);
                        }
                    }
                });
    }

    @Override
    public void changeWorkspace(final Workspace workspace) {
        getMvpView().showLoading();
        if (workspace.getId().equals("0")) {
            getAllTask();
        } else if (workspace.getId().equals("1")) {
            final List<Workspace> workspaces = new ArrayList<>();
            db.collection(TABLE_USER)
                    .document(currentUser.getUid())
                    .collection(USER_WORKSPACE)
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull com.google.android.gms.tasks.Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                for (DocumentSnapshot documentSnapshot : task.getResult()) {
                                    workspaces.add(documentSnapshot.toObject(Workspace.class));
                                    Log.d("WORKSPACE_LIST1", documentSnapshot.toObject(Workspace.class).getName());
                                }
                                Log.d("WORKSPACE_LIST1", String.valueOf(task.getResult().size()));
                                if (task.getResult().size() == 0) {
                                    getMvpView().getAllTask(arrTasks);
                                } else {
                                    arrTasks.clear();
                                }
                                for (Workspace workspace : workspaces) {
                                    db.collection(USER_WORKSPACE)
                                            .document(workspace.getId())
                                            .collection(TABLE_USER)
                                            .get()
                                            .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                @Override
                                                public void onComplete(@NonNull com.google.android.gms.tasks.Task<QuerySnapshot> task) {
                                                    if (task.isSuccessful()) {
                                                        for (DocumentSnapshot documentSnapshot : task.getResult()) {
                                                            User user = documentSnapshot.toObject(User.class);
                                                            Log.d("USER_LIST1", user.getName());
                                                            db.collection(TABLE_COLLECTION)
                                                                    .whereEqualTo("user_id", user.getId())
                                                                    .get()
                                                                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                                        @Override
                                                                        public void onComplete(@NonNull com.google.android.gms.tasks.Task<QuerySnapshot> task) {
                                                                            if (task.isSuccessful()) {
                                                                                for (DocumentSnapshot documentSnapshot1 : task.getResult()) {
                                                                                    Task note = documentSnapshot1.toObject(Task.class);
                                                                                    boolean check = false;
                                                                                    for (Task check_note : arrTasks) {
                                                                                        if (check_note.getId().equals(note.getId())) {
                                                                                            check = true;
                                                                                            break;
                                                                                        }
                                                                                    }
                                                                                    if (!check)
                                                                                        arrTasks.add(note);

                                                                                }
                                                                                getMvpView().getAllTask(arrTasks);
                                                                            }
                                                                        }
                                                                    });
                                                        }

                                                    }
                                                }
                                            });
                                }
                                getMvpView().hideLoading();
                            } else {
                                getMvpView().hideLoading();
                                getMvpView().getAllTask(arrTasks);
                            }
                        }
                    });

        } else {
//            db.collection(USER_WORKSPACE)
//                    .document(workspace.getId())
//                    .collection(TABLE_USER)
//                    .get()
//                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
//                        @Override
//                        public void onComplete(@NonNull com.google.android.gms.tasks.Task<QuerySnapshot> task) {
//                            if (task.isSuccessful()) {
//                                arrTasks.clear();
//                                for (DocumentSnapshot documentSnapshot : task.getResult()) {
//                                    User user = documentSnapshot.toObject(User.class);
//                                    Log.d("USER_LIST1", user.getName());
//                                    db.collection(TABLE_COLLECTION)
//                                            .whereEqualTo("user_id", user.getId())
//                                            .get()
//                                            .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
//                                                @Override
//                                                public void onComplete(@NonNull com.google.android.gms.tasks.Task<QuerySnapshot> task) {
//                                                    if (task.isSuccessful()) {
//                                                        getMvpView().hideLoading();
//                                                        for (DocumentSnapshot documentSnapshot1 : task.getResult()) {
//                                                            Task note = documentSnapshot1.toObject(Task.class);
//                                                            arrTasks.add(note);
//                                                            Log.d("NOTE_LIST1", note.getText());
//                                                        }
//
//                                                    }
//                                                }
//                                            });
//                                }
//
//                            }
//                        }
//                    });
            db.collection(USER_WORKSPACE)
                    .document(workspace.getId())
                    .collection(TABLE_USER)
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull com.google.android.gms.tasks.Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                arrTasks.clear();
                                db.collection(USER_WORKSPACE)
                                        .document(workspace.getId())
                                        .collection(TABLE_COLLECTION)
                                        .get()
                                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull com.google.android.gms.tasks.Task<QuerySnapshot> task1) {
                                                if (task1.isSuccessful()) {
                                                    for (DocumentSnapshot documentSnapshot2 : task1.getResult()) {
                                                        Task note = documentSnapshot2.toObject(Task.class);
                                                        arrTasks.add(note);
                                                        Log.d("NOTE_LIST3", note.getText());
                                                    }
                                                    getMvpView().getAllTask(arrTasks);
                                                }
                                            }
                                        });
                                for (DocumentSnapshot documentSnapshot : task.getResult()) {
                                    User user = documentSnapshot.toObject(User.class);
                                    Log.d("USER_LIST3", user.getName());
                                    db.collection(TABLE_COLLECTION)
                                            .whereEqualTo("user_id", user.getId())
                                            .get()
                                            .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                @Override
                                                public void onComplete(@NonNull com.google.android.gms.tasks.Task<QuerySnapshot> task) {
                                                    if (task.isSuccessful()) {
                                                        for (DocumentSnapshot documentSnapshot1 : task.getResult()) {
                                                            Task note = documentSnapshot1.toObject(Task.class);
                                                            arrTasks.add(note);
                                                            Log.d("NOTE_LIST3", note.getText());
                                                        }
                                                        getMvpView().getAllTask(arrTasks);
                                                    }
                                                }
                                            });
                                }
                                getMvpView().hideLoading();
                            } else {
                                arrTasks.clear();
                                getMvpView().getAllTask(arrTasks);
                            }
                        }
                    });
        }
    }

    @Override
    public void deleteTask(Task task) {
        db.collection(TABLE_COLLECTION)
                .document(task.getId())
                .delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

            }
        });
        getAllTask();
    }

    @Override
    public void loadUserInformation() {
        db.collection(TABLE_USER)
                .document(currentUser.getUid())
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull com.google.android.gms.tasks.Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            assert document != null;
                            User user = document.toObject(User.class);
                            assert user != null;
                            if (user.getDate_of_birth() == null) {
                                user.setDate_of_birth("");
                            }
                            if (user.getFavorite() == null) {
                                user.setFavorite("");
                            }
                            if (user.getAddress() == null) {
                                user.setAddress("");
                            }
                            if (user.getWork() == null) {
                                user.setWork("");
                            }
                            getMvpView().loadUserInformation(user);
                        }
                    }
                });
    }

    @Override
    public void getAllWorkspaceTask(final Workspace workspace) {
        db.collection(USER_WORKSPACE)
                .document(workspace.getId())
                .collection(TABLE_COLLECTION)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull com.google.android.gms.tasks.Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            arrTasks.clear();
                            for (DocumentSnapshot document : task.getResult()) {
                                Task note = document.toObject(Task.class);
                                assert note != null;
                                if (currentUser.getUid().equals(note.getUser_id())) {
                                    arrTasks.add(note);
                                }
                                Log.d("DB_TAG", note.getText());
                            }
                            getMvpView().getAllTask(arrTasks);
                        }
                    }
                });

    }

    @Override
    public void deleteWorkspaceTask(final Workspace workspace, Task note) {
        getMvpView().showLoading();
        db.collection(USER_WORKSPACE)
                .document(workspace.getId())
                .collection(TABLE_COLLECTION)
                .document(note.getId())
                .delete()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull com.google.android.gms.tasks.Task<Void> task) {
                        if (task.isSuccessful()) {
                            getMvpView().hideLoading();
                            getAllWorkspaceTask(workspace);
                        }
                    }
                });
    }


}
