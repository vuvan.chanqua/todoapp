package com.example.myapplication.mvp.ui.workspace;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.myapplication.mvp.R;
import com.example.myapplication.mvp.data.firebase.User;
import com.example.myapplication.mvp.data.firebase.Workspace;
import com.example.myapplication.mvp.ui.base.BaseActivity;
import com.example.myapplication.mvp.ui.workspace.adapter.pager.PagerAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddWorkspaceActivity extends BaseActivity implements WorkspaceMvpView {

    private static final String WORKSPACE_OBJECT = "WORKSPACE_OBJECT";

    @Inject
    WorkspacePresenter<WorkspaceMvpView> mPresenter;

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_workspace);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(AddWorkspaceActivity.this);
        setupToolbar();
        setupTabLayout();
    }

    @Override
    protected void setUp() {

    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbarTitle = toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setTextSize(22);
        toolbarTitle.setText(getResources().getString(R.string.workspace));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void setupTabLayout() {
        tabLayout.addTab(tabLayout.newTab().setText("Tab 1"));

        final Intent data = getIntent();
        final Workspace workspace = (Workspace) data.getSerializableExtra(WORKSPACE_OBJECT);
        if (workspace != null) {
            tabLayout.addTab(tabLayout.newTab().setText("Tab 2"));
            tabLayout.addTab(tabLayout.newTab().setText("Tab 3"));
        }
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.bg_color));
        final ViewPager viewPager = findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public void backActivity() {
        Intent intent = new Intent(this, WorkspaceActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            backActivity();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void getAllWorkspace(List<Workspace> workspaces) {

    }

    @Override
    public void onSuccessActivity() {

    }

    @Override
    public void listUser(List<User> users) {

    }

    @Override
    public void listUserChose(List<User> allUsers, List<User> users) {

    }
}
