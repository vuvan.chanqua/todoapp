package com.example.myapplication.mvp.ui.profile;


import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.myapplication.mvp.data.DataManager;
import com.example.myapplication.mvp.data.firebase.User;
import com.example.myapplication.mvp.ui.base.BasePresenter;
import com.example.myapplication.mvp.utils.rx.SchedulerProvider;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

import static com.example.myapplication.mvp.utils.AppConstants.TABLE_USER;

public class ProfilePresenter<V extends ProfileMvpView> extends BasePresenter<V> implements ProfileMvpPresenter<V> {

    private static final String TAG = "ProfilePresenter";
    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private StorageReference storageRef;

    @Inject
    public ProfilePresenter(DataManager dataManager,
                            SchedulerProvider schedulerProvider,
                            CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
        this.mAuth = FirebaseAuth.getInstance();
        this.db = FirebaseFirestore.getInstance();
        this.currentUser = mAuth.getCurrentUser();
        this.storageRef = FirebaseStorage.getInstance().getReference();
    }

    @Override
    public void changePassword(String oldPwd, final String newPwd) {
        getMvpView().showLoading();
        AuthCredential credential = EmailAuthProvider
                .getCredential(currentUser.getEmail(), oldPwd);
        currentUser.reauthenticate(credential)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            currentUser.updatePassword(newPwd).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Log.d(TAG, "Password updated");
                                        Map<String, Object> objectUser = new HashMap<>();

                                        objectUser.put("password", newPwd);
                                        userUpdate(objectUser);
                                        getMvpView().checkPasswordSuccess();
                                        getMvpView().hideLoading();
                                    } else {
                                        Log.d(TAG, "Error password not updated");
                                    }
                                }
                            });
                        } else {
                            getUserInformation();
                        }
                    }
                });
    }

    @Override
    public void saveInformation(String usn, String name, String imgUri, String date_birth,
                                String favorite, String address, String work, int type) {
        if (!imgUri.equals("") && !imgUri.equals(currentUser.getPhotoUrl())) {
            Log.d(TAG, "information with image");
            saveInformationWithImage(usn, name, imgUri, date_birth, favorite, address, work, type);
        } else {
            Log.d(TAG, "information with no image");
            saveInformationNoImage(usn, name, imgUri, date_birth, favorite, address, work, type);
        }
    }

    private void saveInformationNoImage(final String usn, final String name
            , String imgUri, final String dateBirth
            , final String favorite, final String address
            , final String work, final int type) {
        Log.d(TAG, "User profile updated.");
        getMvpView().showLoading();
        currentUser.updateEmail(usn).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    final Map<String, Object> objectUser = new HashMap<>();
                    objectUser.put("email", currentUser.getEmail());
                    objectUser.put("date_of_birth", dateBirth);
                    objectUser.put("favorite", favorite);
                    objectUser.put("address", address);
                    objectUser.put("work", work);
                    objectUser.put("type", type);
                    userUpdate(objectUser);
                    getMvpView().hideLoading();
                    Log.d(TAG, "information updated");
                } else {
                    Log.d(TAG, "Error Email not updated");
                }
            }
        });

        UserProfileChangeRequest profileUpdate = new UserProfileChangeRequest.Builder()
                .setDisplayName(name)
                .build();
        currentUser.updateProfile(profileUpdate).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                final Map<String, Object> objectUser = new HashMap<>();
                objectUser.put("name", currentUser.getDisplayName());
                userUpdate(objectUser);
                getMvpView().saveInformationSuccess();
            }
        });
    }

    private void saveInformationWithImage(final String usn, final String name
            , final String imgUri, final String dateBirth
            , final String favorite, final String address
            , final String work, final int type) {
        String img_profile = Long.toString(System.currentTimeMillis());
        final StorageReference profile_img = storageRef.child("profile_images").child(img_profile + ".png");
        profile_img.putFile(Uri.parse(imgUri)).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull final com.google.android.gms.tasks.Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()) {
                    profile_img.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(final Uri uri) {
                            UserProfileChangeRequest profileUpdate = new UserProfileChangeRequest.Builder()
                                    .setPhotoUri(uri)
                                    .build();
                            currentUser.updateProfile(profileUpdate).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    final Map<String, Object> objectUser = new HashMap<>();
                                    objectUser.put("profile_img", uri.toString());
                                    userUpdate(objectUser);
                                }
                            });
                            saveInformationNoImage(usn, name, imgUri, dateBirth, favorite, address, work, type);
                        }
                    });
                } else {
                    getMvpView().saveInformationFailed();
                }
            }
        });
    }

    private void userUpdate(Map<String, Object> objectUser) {
        db.collection(TABLE_USER)
                .document(currentUser.getUid())
                .update(objectUser)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "update user");
                    }
                });
    }

    @Override
    public void getUserInformation() {
        db.collection(TABLE_USER)
                .document(currentUser.getUid())
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            assert document != null;
                            User user = document.toObject(User.class);
                            assert user != null;
                            if (user.getDate_of_birth() == null) {
                                user.setDate_of_birth("");
                            }
                            if (user.getFavorite() == null) {
                                user.setFavorite("");
                            }
                            if (user.getAddress() == null) {
                                user.setAddress("");
                            }
                            if (user.getWork() == null) {
                                user.setWork("");
                            }
                            getMvpView().loadUserInformation(user);
                        }
                    }
                });
    }
}