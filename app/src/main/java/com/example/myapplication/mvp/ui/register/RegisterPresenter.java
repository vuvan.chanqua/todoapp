package com.example.myapplication.mvp.ui.register;


import android.support.annotation.NonNull;
import android.util.Log;

import com.example.myapplication.mvp.data.DataManager;
import com.example.myapplication.mvp.ui.base.BasePresenter;
import com.example.myapplication.mvp.utils.rx.SchedulerProvider;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

import static com.example.myapplication.mvp.utils.AppConstants.TABLE_USER;

public class RegisterPresenter<V extends RegisterMvpView> extends BasePresenter<V> implements RegisterMvpPresenter<V> {

    private static final String TAG = "RegisterPresenter";
    private FirebaseFirestore db;
    private FirebaseAuth mAuth;

    @Inject
    public RegisterPresenter(DataManager dataManager,
                             SchedulerProvider schedulerProvider,
                             CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
        this.db = FirebaseFirestore.getInstance();
        this.mAuth = FirebaseAuth.getInstance();

    }

    @Override
    public void register(final String email, final String password, final String name) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            final FirebaseUser user = mAuth.getCurrentUser();
                            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                    .setDisplayName(name).build();
                            assert user != null;
                            user.updateProfile(profileUpdates)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Log.d(TAG, "User profile updated.");
                                                Map<String, Object> objectUser = new HashMap<>();
                                                objectUser.put("id", user.getUid());
                                                objectUser.put("email", email);
                                                objectUser.put("type", 0);
                                                objectUser.put("name", name);
                                                objectUser.put("password", password);

                                                db.collection(TABLE_USER)
                                                        .document(user.getUid())
                                                        .set(objectUser)
                                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                            @Override
                                                            public void onSuccess(Void aVoid) {
                                                                Log.d(TAG, "create tag");
                                                            }
                                                        });
                                            }
                                        }
                                    });
                            getMvpView().registerSuccess();
                        }
                        else{
                            getMvpView().registerFailed();
                        }
                    }
                });


    }
}