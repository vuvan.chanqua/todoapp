

package com.example.myapplication.mvp.data.network;

import com.example.myapplication.mvp.data.network.model.BlogResponse;
import com.example.myapplication.mvp.data.network.model.LoginRequest;
import com.example.myapplication.mvp.data.network.model.LoginResponse;
import com.example.myapplication.mvp.data.network.model.LogoutResponse;
import com.example.myapplication.mvp.data.network.model.OpenSourceResponse;

import io.reactivex.Single;



public interface ApiHelper {

    ApiHeader getApiHeader();

    Single<LoginResponse> doGoogleLoginApiCall(LoginRequest.GoogleLoginRequest request);

    Single<LoginResponse> doFacebookLoginApiCall(LoginRequest.FacebookLoginRequest request);

    Single<LoginResponse> doServerLoginApiCall(LoginRequest.ServerLoginRequest request);

    Single<LogoutResponse> doLogoutApiCall();

    Single<BlogResponse> getBlogApiCall();

    Single<OpenSourceResponse> getOpenSourceApiCall();
}
