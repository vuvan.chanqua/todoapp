package com.example.myapplication.mvp.ui.workspace;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.myapplication.mvp.R;
import com.example.myapplication.mvp.data.firebase.User;
import com.example.myapplication.mvp.data.firebase.Workspace;
import com.example.myapplication.mvp.ui.base.BaseActivity;
import com.example.myapplication.mvp.ui.main.MainActivity;
import com.example.myapplication.mvp.ui.workspace.adapter.workspace.WorkspaceAdapter;
import com.example.myapplication.mvp.ui.workspace.adapter.workspace.WorkspaceListener;
import com.example.myapplication.mvp.utils.SharedPrefs;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.myapplication.mvp.utils.AppConstants.CURRENT_USER;

public class WorkspaceActivity extends BaseActivity implements WorkspaceMvpView, WorkspaceListener {

    @Inject
    WorkspacePresenter<WorkspaceMvpView> mPresenter;

    @BindView(R.id.addwpBtn)
    Button addBtn;

    @BindView(R.id.workspaceList)
    RecyclerView recyclerView;

    private static final String WORKSPACE_OBJECT = "WORKSPACE_OBJECT";
    private User userInfo;
    List<Workspace> workspacesList;
    WorkspaceAdapter workspaceAdapter;
    SharedPreferences mPrefs;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, WorkspaceActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workspace);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(WorkspaceActivity.this);
        setupToolbar();
        setUp();
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setTextSize(22);
        toolbarTitle.setText(getResources().getString(R.string.workspace));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {
        mPrefs = getPreferences(MODE_PRIVATE);
        userInfo = SharedPrefs.getInstance().get(CURRENT_USER, User.class);
        mPresenter.getAllWorkspace(userInfo);
        addBtn.setVisibility(View.GONE);
        if (userInfo.getType() == 1) {
            addBtn.setVisibility(View.VISIBLE);
            addBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(WorkspaceActivity.this, AddWorkspaceActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
            });
        } else {
            addBtn.setVisibility(View.GONE);
        }

        workspacesList = new ArrayList<>();
        workspaceAdapter = new WorkspaceAdapter(workspacesList);
        workspaceAdapter.setWorkspacesButtonListner(WorkspaceActivity.this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(workspaceAdapter);
    }

    @Override
    public void getAllWorkspace(List<Workspace> workspaces) {
        workspacesList.clear();
        workspacesList.addAll(workspaces);
        recyclerView.setAdapter(workspaceAdapter);
    }

    @Override
    public void onSuccessActivity() {
        mPresenter.getAllWorkspace(userInfo);
    }

    @Override
    public void listUser(List<User> users) {

    }

    @Override
    public void listUserChose(List<User> allUsers, List<User> users) {

    }

    @Override
    public void onButtonEditClickListner(int position) {
        Workspace workspace = workspacesList.get(position);
        Intent intent = new Intent(WorkspaceActivity.this, AddWorkspaceActivity.class);
        intent.putExtra(WORKSPACE_OBJECT, workspace);
        Log.d("WORKSPACE_OBJECT", workspace.getName());
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @Override
    public void onButtonOutClickListner(Workspace workspace) {
        mPresenter.exitWorkspace(workspace);
    }

    @Override
    public void onButtonDeleteClickListner(Workspace workspace) {
        mPresenter.deleteWorkspace(workspace);
    }
}