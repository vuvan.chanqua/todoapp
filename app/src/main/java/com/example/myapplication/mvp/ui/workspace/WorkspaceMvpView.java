package com.example.myapplication.mvp.ui.workspace;

import com.example.myapplication.mvp.data.firebase.User;
import com.example.myapplication.mvp.data.firebase.Workspace;
import com.example.myapplication.mvp.ui.base.MvpView;

import java.util.List;

public interface WorkspaceMvpView extends MvpView {
    void getAllWorkspace(List<Workspace> workspaces);

    void onSuccessActivity();

    void listUser(List<User> users);

    void listUserChose(List<User> allUsers, List<User> users);
}