package com.example.myapplication.mvp.ui.add_note;

import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.myapplication.mvp.R;
import com.example.myapplication.mvp.data.firebase.Task;
import com.example.myapplication.mvp.data.firebase.Workspace;
import com.example.myapplication.mvp.ui.base.BaseActivity;
import com.example.myapplication.mvp.utils.KeyboardUtils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddNoteActivity extends BaseActivity implements AddNoteMvpView {

    private static final int PICK_IMAGE = 1;
    private static final String WORKSPACE_OBJECT = "WORKSPACE_OBJECT";

    @BindView(R.id.editText2)
    EditText editText;

    @BindView(R.id.editText3)
    EditText previewText;

    @BindView(R.id.red_btn)
    Button red_btn;

    @BindView(R.id.green_btn)
    Button green_btn;

    @BindView(R.id.yallow_btn)
    Button yallow_btn;

    @BindView(R.id.sub_btn)
    Button sub_btn;

    @BindView(R.id.imageView)
    ImageView imageView;

    @BindView(R.id.spinner)
    Spinner spinner;

    @BindView(R.id.swPreview)
    Switch swPreview;

    ImageView[] imageBtn = new ImageView[3];
    @BindView(R.id.imageBtn1)
    ImageView imageBtn1;

    @BindView(R.id.imageBtn2)
    ImageView imageBtn2;

    @BindView(R.id.imageBtn3)
    ImageView imageBtn3;

    @BindView(R.id.imagePicker)
    Button imagePicker;

    @BindView(R.id.boldBtn)
    Button boldBtn;

    @BindView(R.id.italicBtn)
    Button italicBtn;

    @BindView(R.id.linkBtn)
    Button linkBtn;

    @BindView(R.id.quoteBtn)
    Button quoteBtn;

    @BindView(R.id.unlBtn)
    Button unlBtn;

    @BindView(R.id.brBtn)
    Button brBtn;

    @Inject
    AddNotePresenter<AddNoteMvpView> mPresenter;

    private String fromActivity;
    private Workspace mWorkspace;
    private int bgColor;
    private String t_id = "-1";
    private float txSize;

    List<String> list_spinner = new ArrayList<>();
    ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
    public static String[] EXTRA_URL = {"", "", ""};
    FirebaseAuth mAuth;
    FirebaseUser currentUser;
    private int typeTask;
    private int positionWorkspace;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, AddNoteActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);

        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(AddNoteActivity.this);
        setUp();
        setupToolbar();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {

        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();

        final Intent intent = getIntent();
        fromActivity = intent.getStringExtra("activity");
        positionWorkspace = intent.getIntExtra("position_workspace", 0);
        Log.d("WORKSPACE_POSITION", String.valueOf(positionWorkspace));
        final Intent data = getIntent();
        final Workspace workspace = (Workspace) data.getSerializableExtra(WORKSPACE_OBJECT);

        if (workspace != null) {
            mWorkspace = workspace;
        }

        fontSizeChange();
        final Task task = (Task) intent.getSerializableExtra("TASK_OBJECT");

        if (task != null) {
            editText.setText(task.getText());
            editText.setTextSize(task.getFont_size());
            imageView.setColorFilter(task.getColor());
            bgColor = task.getColor();
            t_id = task.getId();
            typeTask = task.getType();
            String txSize = Integer.toString(Math.round(task.getFont_size()));
            Log.d("INFO_TAG", t_id);

            for (int i = 0; i < list_spinner.size(); i++) {
                if (list_spinner.get(i).equals(txSize)) {
                    spinner.setSelection(i);
                }
            }
            if (!task.getImg_url_1().equals("")) {
                Glide.with(AddNoteActivity.this)
                        .load(task.getImg_url_1())
                        .into(imageBtn1);
            }
            if (!task.getImg_url_2().equals("")) {
                Glide.with(AddNoteActivity.this)
                        .load(task.getImg_url_2())
                        .into(imageBtn2);
            }
            if (!task.getImg_url_3().equals("")) {
                Glide.with(AddNoteActivity.this)
                        .load(task.getImg_url_3())
                        .into(imageBtn3);
            }
        }


        previewText.setVisibility(View.GONE);
        previewText.setEnabled(false);
        selectBackground();

        swPreview.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                String tmp_str = editText.getText().toString();
                if (isChecked) {
                    previewText.setVisibility(View.VISIBLE);
                    editText.setVisibility(View.GONE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        previewText.setText(Html.fromHtml(editText.getText().toString(), Html.FROM_HTML_MODE_COMPACT));
                        previewText.setTextSize(txSize);
                    } else {
                        previewText.setText(Html.fromHtml(editText.getText().toString()));
                        previewText.setTextSize(txSize);

                    }
                } else {
                    previewText.setVisibility(View.GONE);
                    editText.setVisibility(View.VISIBLE);
                }
            }
        });

        imagePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });


        boldBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int start = editText.getSelectionStart();
                editText.getText().insert(start, "<b></b>");
                editText.setSelection(start + 3);
                KeyboardUtils.showSoftInput(editText, getApplicationContext());
            }
        });
        italicBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int start = editText.getSelectionStart();
                editText.getText().insert(start, "<i></i>");
                editText.setSelection(start + 3);
                KeyboardUtils.showSoftInput(editText, getApplicationContext());

            }
        });
        linkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int start = editText.getSelectionStart();
                editText.getText().insert(start, "<a href=''></a>");
                editText.setSelection(start + 11);
                KeyboardUtils.showSoftInput(editText, getApplicationContext());

            }
        });
        quoteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int start = editText.getSelectionStart();
                editText.getText().insert(start, "<q></q>");
                editText.setSelection(start + 3);
                KeyboardUtils.showSoftInput(editText, getApplicationContext());

            }
        });
        unlBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int start = editText.getSelectionStart();
                editText.getText().insert(start, "<u></u>");
                editText.setSelection(start + 3);
                KeyboardUtils.showSoftInput(editText, getApplicationContext());

            }
        });
        brBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int start = editText.getSelectionStart();
                editText.getText().insert(start, "<br>");
                KeyboardUtils.showSoftInput(editText, getApplicationContext());
            }
        });


        sendData();
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setTextSize(22);
        toolbarTitle.setText(getResources().getString(R.string.note));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (fromActivity.equals("main")) {
                final Intent data = new Intent();
                data.putExtra("position_workspace", positionWorkspace);
                setResult(Activity.RESULT_OK, data);
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            } else {
                final Intent data = new Intent();
                setResult(Activity.RESULT_OK, data);
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void selectImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
    }

    public void selectBackground() {
        red_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageView.setColorFilter(getResources().getColor(R.color.colorPink));
                bgColor = getResources().getColor(R.color.colorPink);
                Log.d("COLOR_TAG", "red");
            }
        });

        green_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageView.setColorFilter(getResources().getColor(R.color.colorGreen));
                bgColor = getResources().getColor(R.color.colorGreen);
            }
        });

        yallow_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageView.setColorFilter(getResources().getColor(R.color.colorYallow));
                bgColor = getResources().getColor(R.color.colorYallow);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK && data != null) {
//            Uri uri = data.getData();
            //            imageBtn1.setImageURI(uri);

            if (data.getData() != null) {
                Uri mImageUri = data.getData();
                mArrayUri.add(mImageUri);
                imageBtn[0].setImageURI(mImageUri);
            } else {
                if (data.getClipData() != null) {
                    ClipData mClipData = data.getClipData();

                    for (int i = 0; i < 3; i++) {
                        ClipData.Item item = mClipData.getItemAt(i);
                        Uri uri = item.getUri();
                        mArrayUri.add(uri);
                        if (i == 0) {
                            Glide.with(getApplicationContext()).load(uri).into(imageBtn1);
                        }
                        if (i == 1) {
                            Glide.with(getApplicationContext()).load(uri).into(imageBtn2);
                        }
                        if (i == 2) {
                            Glide.with(getApplicationContext()).load(uri).into(imageBtn3);
                        }
//                        imageBtn[i].setImageURI(uri);
                    }

                    Log.d("MainActivity", "Selected Images" + mArrayUri.size());
                }
            }
        } else {
            Log.d("CODE_REQUEST", "request code" + requestCode);

        }
    }

    public void fontSizeChange() {
        list_spinner.add(Integer.toString(getResources().getInteger(R.integer.tx_16)));
        list_spinner.add(Integer.toString(getResources().getInteger(R.integer.tx_18)));
        list_spinner.add(Integer.toString(getResources().getInteger(R.integer.tx_20)));
        list_spinner.add(Integer.toString(getResources().getInteger(R.integer.tx_24)));


        ArrayAdapter<String> adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, list_spinner);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Float text_size = Float.parseFloat(spinner.getSelectedItem().toString());
                editText.setTextSize(text_size);
                txSize = text_size;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void sendData() {
        sub_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mArrayUri.size() != 0) {
                    for (int i = 0; i < mArrayUri.size(); i++) {
                        EXTRA_URL[i] = mArrayUri.get(i).toString();
                    }
                }
                if (fromActivity.equals("main")) {
                    if (t_id.equals("-1")) {
                        String task_id = String.valueOf(System.currentTimeMillis());
                        Task task = new Task(task_id, currentUser.getUid(), editText.getText().toString(), EXTRA_URL[0], EXTRA_URL[1], EXTRA_URL[2], bgColor, txSize);
                        mPresenter.createTask(task);
                    } else {
                        Task task = new Task(t_id, currentUser.getUid(), editText.getText().toString(), EXTRA_URL[0], EXTRA_URL[1], EXTRA_URL[2], bgColor, txSize);
                        if (typeTask == 0) {
                            mPresenter.updateTask(task);
                        } else {
                            mPresenter.updateWorkspaceTask(mWorkspace, task);
                        }

                    }
                } else {
                    if (t_id.equals("-1")) {
                        String task_id = String.valueOf(System.currentTimeMillis());
                        Task task = new Task(task_id, currentUser.getUid(), editText.getText().toString(), EXTRA_URL[0], EXTRA_URL[1], EXTRA_URL[2], bgColor, txSize);
                        mPresenter.createWorkspaceTask(mWorkspace, task);
                    } else {
                        Task task = new Task(t_id, currentUser.getUid(), editText.getText().toString(), EXTRA_URL[0], EXTRA_URL[1], EXTRA_URL[2], bgColor, txSize);
                        mPresenter.updateWorkspaceTask(mWorkspace, task);
                    }
                }

            }
        });
    }

    @Override
    public void createSuccess() {
        if (fromActivity.equals("main")) {
            final Intent data = new Intent();
            data.putExtra("position_workspace", positionWorkspace);
            setResult(Activity.RESULT_OK, data);
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        } else {
            final Intent data = new Intent();
            data.putExtra("position_workspace", positionWorkspace);
            setResult(Activity.RESULT_OK, data);
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }
    }

    @Override
    public void updateSuccess() {
        Toast.makeText(getApplicationContext(), "Successful", Toast.LENGTH_LONG).show();
    }
}