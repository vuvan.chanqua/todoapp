package com.example.myapplication.mvp.ui.workspace.adapter.workspace;

import com.example.myapplication.mvp.data.firebase.Workspace;

public interface WorkspaceListener {
    void onButtonDeleteClickListner(Workspace workspace);

    void onButtonEditClickListner(int position);

    void onButtonOutClickListner(Workspace workspace);
}
