

package com.example.myapplication.mvp.di.component;

import com.example.myapplication.mvp.di.PerActivity;
import com.example.myapplication.mvp.di.module.ActivityModule;
import com.example.myapplication.mvp.ui.add_note.AddNoteActivity;
import com.example.myapplication.mvp.ui.login.LoginActivity;
import com.example.myapplication.mvp.ui.main.MainActivity;
import com.example.myapplication.mvp.ui.profile.PasswordActivity;
import com.example.myapplication.mvp.ui.profile.ProfileActivity;
import com.example.myapplication.mvp.ui.register.RegisterActivity;
import com.example.myapplication.mvp.ui.splash.SplashActivity;
import com.example.myapplication.mvp.ui.workspace.AddWorkspaceActivity;
import com.example.myapplication.mvp.ui.workspace.WorkspaceActivity;
import com.example.myapplication.mvp.ui.workspace.fragment.TabFragment2;
import com.example.myapplication.mvp.ui.workspace.fragment.TabFragment1;
import com.example.myapplication.mvp.ui.workspace.fragment.TabFragment3;

import dagger.Component;



@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity activity);

    void inject(SplashActivity activity);

    void inject(LoginActivity activity);

    void inject(RegisterActivity activity);

    void inject(WorkspaceActivity activity);

    void inject(ProfileActivity activity);

    void inject(AddNoteActivity activity);

    void inject(PasswordActivity activity);

    void inject(AddWorkspaceActivity activity);

    void inject(TabFragment3 fragment);
    void inject(TabFragment2 fragment);
    void inject(TabFragment1 fragment);

}
