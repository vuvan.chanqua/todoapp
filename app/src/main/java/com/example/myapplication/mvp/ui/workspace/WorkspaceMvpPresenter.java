package com.example.myapplication.mvp.ui.workspace;

import com.example.myapplication.mvp.data.firebase.User;
import com.example.myapplication.mvp.data.firebase.Workspace;
import com.example.myapplication.mvp.ui.base.MvpPresenter;

import java.util.List;

public interface WorkspaceMvpPresenter<V extends WorkspaceMvpView> extends MvpPresenter<V> {
    void getAllWorkspace(User user);

    void addWorkspace(Workspace workspace);

    void updateWokspace(Workspace workspace);

    void deleteWorkspace(Workspace workspace);

    void exitWorkspace(Workspace workspace);

    void addUserWorkspace(Workspace workspace, List<User> users);

    void getListUserByWorkspace(Workspace workspace);

    void getListUser();
}