package com.example.myapplication.mvp.ui.workspace.adapter.workspace_user;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.example.myapplication.mvp.R;
import com.example.myapplication.mvp.data.firebase.User;

import java.util.List;

public class WorkspaceUserAdapter extends RecyclerView.Adapter<WorkspaceUserAdapter.WorkspaceUserViewHolder> {

    private List<User> users;
    private WorkspaceUserListener workspaceUserListener;

    public void setWorkspacesUserButtonListner(WorkspaceUserListener workspaceUserListener) {
        this.workspaceUserListener = workspaceUserListener;
    }

    public WorkspaceUserAdapter(List<User> users) {
        this.users = users;
    }

    @NonNull
    @Override
    public WorkspaceUserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_user_workspace, parent, false);
        return new WorkspaceUserAdapter.WorkspaceUserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final WorkspaceUserViewHolder holder, final int position) {
        final User user = users.get(position);
        if (user.getName() != null) {
        } else {
            holder.textView.setText(user.getEmail());

        }
        holder.textView.setText(user.getName());

        holder.wpEmailText.setText(user.getEmail());
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    workspaceUserListener.onSelectUser(position);
                } else {
                    workspaceUserListener.onUnSelectUser(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class WorkspaceUserViewHolder extends RecyclerView.ViewHolder {
        CheckBox checkBox;
        TextView textView, wpEmailText;

        public WorkspaceUserViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.wpUserText);
            wpEmailText = (TextView) itemView.findViewById(R.id.wpEmailText);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);
        }
    }
}
