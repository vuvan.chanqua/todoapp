package com.example.myapplication.mvp.ui.workspace.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.myapplication.mvp.R;
import com.example.myapplication.mvp.data.firebase.User;
import com.example.myapplication.mvp.data.firebase.Workspace;
import com.example.myapplication.mvp.ui.base.BaseFragment;
import com.example.myapplication.mvp.ui.workspace.WorkspaceActivity;
import com.example.myapplication.mvp.ui.workspace.WorkspaceMvpView;
import com.example.myapplication.mvp.ui.workspace.WorkspacePresenter;
import com.example.myapplication.mvp.utils.SharedPrefs;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.ButterKnife;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static com.example.myapplication.mvp.utils.AppConstants.CURRENT_USER;

public class TabFragment1 extends BaseFragment implements WorkspaceMvpView {

    private static final String WORKSPACE_OBJECT = "WORKSPACE_OBJECT";
    ImageButton imageView;
    EditText editText;
    Button button;
    private static final int PICK_IMAGE = 1;
    private String imgUri = "";
    private String id = "-1";
    private User userInfo;
    SharedPreferences mPrefs;

    @Inject
    WorkspacePresenter<WorkspaceMvpView> mPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrefs = getActivity().getPreferences(MODE_PRIVATE);
        userInfo = SharedPrefs.getInstance().get(CURRENT_USER, User.class);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        setUp(view);
    }

    @Override
    protected void setUp(View view) {
        button = (Button) view.findViewById(R.id.button2);
        imageView = (ImageButton) view.findViewById(R.id.imageView2);
        editText = (EditText) view.findViewById(R.id.editText);

        getActivityComponent().inject(this);
        setUnBinder( ButterKnife.bind(this, view));
        mPresenter.onAttach(TabFragment1.this);

        final Intent data = getActivity().getIntent();
        final Workspace workspace = (Workspace) data.getSerializableExtra(WORKSPACE_OBJECT);
        if (workspace != null) {
            editText.setText(workspace.getName());
            Glide.with(this).load(workspace.getImg_url()).into(imageView);
            id = workspace.getId();
            imgUri = workspace.getImg_url();
        }
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });
        sendData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tab_fragment_1, container, false);
    }

    @Override
    public void getAllWorkspace(List<Workspace> workspaces) {

    }

    @Override
    public void onSuccessActivity() {
//        backActivity();
        Toast.makeText(getActivity(), "Submitted", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void listUser(List<User> users) {

    }

    @Override
    public void listUserChose(List<User> allUsers, List<User> users) {

    }


    public void sendData() {
//        Toast.makeText(getActivity(), id, Toast.LENGTH_SHORT).show();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(editText.getText().toString())) {
                    editText.setError("Cannot empty");
                } else {
                    if (id.equals("-1")) {
                        Workspace workspace = new Workspace();
                        workspace.setName(editText.getText().toString());
                        workspace.setImg_url(imgUri);
                        workspace.setUser_id(userInfo.getId());
                        mPresenter.addWorkspace(workspace);
                    } else {
                        Workspace workspace = new Workspace(id, editText.getText().toString(), imgUri, userInfo.getId());
                        mPresenter.updateWokspace(workspace);
                    }

                }

            }
        });
    }

    public void selectImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK && data != null) {
            Uri uri = data.getData();
            assert uri != null;
            imgUri = uri.toString();
            Glide.with(getActivity()).load(uri).into(imageView);

        } else {
            Log.d("CODE_REQUEST", "request code" + requestCode);

        }
    }

    public void backActivity() {
        Intent intent = new Intent(getActivity(), WorkspaceActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
