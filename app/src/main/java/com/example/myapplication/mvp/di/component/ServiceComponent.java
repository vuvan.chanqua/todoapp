

package com.example.myapplication.mvp.di.component;

import com.example.myapplication.mvp.di.PerService;
import com.example.myapplication.mvp.di.module.ServiceModule;
import com.example.myapplication.mvp.service.SyncService;

import dagger.Component;

/**
 * Created by janisharali on 01/02/17.
 */

@PerService
@Component(dependencies = ApplicationComponent.class, modules = ServiceModule.class)
public interface ServiceComponent {

    void inject(SyncService service);

}
