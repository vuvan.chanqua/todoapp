

package com.example.myapplication.mvp.ui.main;

import com.example.myapplication.mvp.data.firebase.Task;
import com.example.myapplication.mvp.data.firebase.User;
import com.example.myapplication.mvp.data.firebase.Workspace;
import com.example.myapplication.mvp.ui.base.MvpView;

import java.util.ArrayList;
import java.util.List;


public interface MainMvpView extends MvpView {

    void openLoginActivity();

    void getAllTask(ArrayList<Task> tasks);

    void getAllWorkspace(List<Workspace> workspaces);

    void loadUserInformation(User user);

}
