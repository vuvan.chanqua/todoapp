package com.example.myapplication.mvp.ui.splash;

import com.example.myapplication.mvp.ui.base.MvpView;


public interface SplashMvpView extends MvpView {

    void openLoginActivity();

    void openMainActivity();

    void startSyncService();
}
