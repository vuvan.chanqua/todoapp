package com.example.myapplication.mvp.ui.add_note;

import com.example.myapplication.mvp.ui.base.MvpView;

public interface AddNoteMvpView extends MvpView {
    void createSuccess();

    void updateSuccess();
}