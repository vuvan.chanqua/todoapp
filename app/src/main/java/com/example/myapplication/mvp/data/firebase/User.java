package com.example.myapplication.mvp.data.firebase;

public class User {
    private String id;
    private String email;
    private String name;
    private String password;
    private String date_of_birth;
    private String favorite;
    private String address;
    private String work;
    private String profile_img;
    private int type;

    public User() {
    }

    public User(String id, String email, String name, String password, String date_of_birth,
                String favorite, String address, String work, String profile_img, int type) {
        this.id = id;
        this.email = email;
        this.name = name;
        this.password = password;
        this.date_of_birth = date_of_birth;
        this.favorite = favorite;
        this.address = address;
        this.work = work;
        this.profile_img = profile_img;
        this.type = type;
    }

    public String getProfile_img() {
        return profile_img;
    }

    public void setProfile_img(String profile_img) {
        this.profile_img = profile_img;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getFavorite() {
        return favorite;
    }

    public void setFavorite(String favorite) {
        this.favorite = favorite;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return email;
    }
}
