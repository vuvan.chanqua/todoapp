package com.example.myapplication.mvp.ui.workspace.adapter.workspace_user;

public interface WorkspaceUserListener {
    void onSelectUser(int position);

    void onUnSelectUser(int position);
}
