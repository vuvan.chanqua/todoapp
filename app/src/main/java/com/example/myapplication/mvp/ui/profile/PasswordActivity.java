package com.example.myapplication.mvp.ui.profile;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myapplication.mvp.R;
import com.example.myapplication.mvp.data.firebase.User;
import com.example.myapplication.mvp.ui.base.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PasswordActivity extends BaseActivity implements ProfileMvpView {

    @Inject
    ProfilePresenter<ProfileMvpView> mPresenter;

    @BindView(R.id.oldpassText)
    EditText old_password;

    @BindView(R.id.newpassText)
    EditText new_password;

    @BindView(R.id.subPwdBtn)
    Button sub_button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(PasswordActivity.this);
        setUp();
    }

    @Override
    protected void setUp() {

        sub_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(old_password.getText().toString())) {
                    old_password.setError("Cannot empty");
                }
                if (TextUtils.isEmpty(new_password.getText().toString())) {
                    new_password.setError("Cannot empty");
                }
                if (new_password.getText().toString().equals(old_password.getText().toString())) {
                    new_password.setError("Similar with old password");
                }
                mPresenter.changePassword(old_password.getText().toString(), new_password.getText().toString());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent intent = new Intent(this, ProfileActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void loadUserInformation(User userInfo) {

    }

    @Override
    public void checkPasswordSuccess() {
        Toast.makeText(PasswordActivity.this, "Change password succesful", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(PasswordActivity.this, ProfileActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void checkPasswordFailed() {
        Toast.makeText(PasswordActivity.this, "Change password failed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void saveInformationSuccess() {

    }

    @Override
    public void saveInformationFailed() {

    }

}
