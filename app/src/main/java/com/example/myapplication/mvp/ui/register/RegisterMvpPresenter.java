package com.example.myapplication.mvp.ui.register;

import com.example.myapplication.mvp.ui.base.MvpPresenter;

public interface RegisterMvpPresenter<V extends RegisterMvpView> extends MvpPresenter<V> {
    void register(String email, String password, String name);
}